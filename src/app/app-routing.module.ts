import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PresenzeComponent } from './presenze/presenze.component';
import { AreaComponent } from './area/area.component';
import { PresenzeListComponent } from './presenze/presenze-list/presenze-list.component';
import { PresenzeHomeComponent } from './presenze/presenze-home/presenze-home.component';
import { PresenzeRicercaComponent } from './presenze/presenze-ricerca/presenze-ricerca.component';
import { AssenzaComponent } from './assenza/assenza.component';
import { AssenzaHomeComponent } from './assenza/assenza-home/assenza-home.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthGuard } from './auth/service/auth.guard';
import { ChangeProfileComponent } from './core/change-profile/change-profile.component';
import { AdminComponent } from './admin/admin.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'dipendenti', loadChildren: './dipendente/dipendente.module#DipendenteModule', canActivate: [AuthGuard] },
  { path: 'ospiti', loadChildren: './ospite/ospite.module#OspiteModule', canActivate: [AuthGuard] },
  {
    path: 'presenze', component: PresenzeComponent, canActivate: [AuthGuard], children: [
      { path: '', pathMatch: 'full', component: PresenzeHomeComponent },
      { path: 'report', component: PresenzeRicercaComponent, canActivate: [AuthGuard] }]
  },
  {
    path: 'assenze', component: AssenzaComponent, canActivate: [AuthGuard], children: [
      { path: '', pathMatch: 'full', component: AssenzaHomeComponent },
    ]
  },
  { path: 'aree', canActivate: [AuthGuard], data: { auth: 'ADMIN' }, component: AreaComponent },
  { path: 'admin', canActivate: [AuthGuard], data: { auth: 'ADMIN' }, component: AdminComponent },
  { path: 'changeProfile', canActivate: [AuthGuard], component: ChangeProfileComponent },
  { path: '**', redirectTo: '' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
