import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/user.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/auth/service/auth.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-admin-nuovo-utente',
  templateUrl: './admin-nuovo-utente.component.html',
  styleUrls: ['./admin-nuovo-utente.component.css']
})
export class AdminNuovoUtenteComponent implements OnInit {
  userForm: FormGroup;
  amministratore: boolean;
  finito: boolean;
  constructor(private router: Router, private route: ActivatedRoute, private authService: AuthService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    const nome = '';
    const cognome = '';
    const email = '';
    const password = '';
    this.amministratore = false;
    this.userForm = new FormGroup({
      'nome': new FormControl(nome, Validators.required),
      'cognome': new FormControl(cognome, Validators.required),
      'email': new FormControl(email, Validators.required),
      'password': new FormControl(password, Validators.required),
      'amministratore': new FormControl(this.amministratore, Validators.required),
    });
    this.authService.finitoRegistrazione.subscribe(
      (user) => {

        this.openSnackBar('Utente ' + user.nome + ' con email ' + user.email + 'registrato con successo', '');

      }
    );
    this.authService.errorEvent.subscribe(
      (error) => {
        this.openSnackBar(error.error, '');
      }
    );
  }

  onSubmit() {
    const value = this.userForm.value;
    const user = new User(
      null,
      value.nome,
      value.cognome,
      value.email,
      value.password,
      true,
      this.amministratore);
    this.authService.signupUser(user);
  }

  onClear() {
    this.userForm.reset();
  }
  onBack() {
    this.router.navigate(['dipendenti']);
  }


  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
