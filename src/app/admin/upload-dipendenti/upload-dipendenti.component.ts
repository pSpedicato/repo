import { Component, OnInit } from '@angular/core';
import { ExcelService } from 'src/app/presenze/presenze-list/excel.service';

@Component({
  selector: 'app-upload-dipendenti',
  templateUrl: './upload-dipendenti.component.html',
  styleUrls: ['./upload-dipendenti.component.css']
})
export class UploadDipendentiComponent implements OnInit {
  path: string;
  constructor(private xls: ExcelService) { }

  ngOnInit() {
  }

  fileCaricato(event) {
    this.path = event.target.files[0];
    console.log(this.path);
  }
  onUpload() {
    this.xls.importFromExcel(this.path);
  }

}
