import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatListModule
} from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { AdminComponent } from './admin.component';
import { AdminNuovoUtenteComponent } from './admin-nuovo-utente/admin-nuovo-utente.component';
import { AreaModule } from '../area/area.module';
import { UploadDipendentiComponent } from './upload-dipendenti/upload-dipendenti.component';

@NgModule({
  declarations: [
    AdminComponent,
    AdminNuovoUtenteComponent,
    UploadDipendentiComponent,

  ],
  imports: [
    CommonModule,
    MatSidenavModule,

    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatButtonToggleModule,
    AreaModule,
  ],
  entryComponents: [
  ],
  exports: [
  ]
})
export class AdminModule { }
