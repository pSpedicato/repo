import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AreaComponent } from './area.component';
import { AreaEditComponent } from './area-edit/area-edit.component';
import { MaterialModule } from '../material.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AreaComponent, AreaEditComponent],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule
  ],
  exports: [AreaComponent, AreaEditComponent],

})
export class AreaModule { }
