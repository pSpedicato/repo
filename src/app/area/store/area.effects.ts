import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Router } from '@angular/router';
import * as AreeActions from './area.actions';
import * as fromAree from './area.reducers';
import { switchMap, map, withLatestFrom } from 'rxjs/operators';
import { from } from 'rxjs';
import { HttpClient, HttpRequest, HttpResponse, HttpParams } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Area } from 'src/app/shared/area.model';

@Injectable()
export class AreaEffects {
    @Effect()
    getAree = this.actions$.ofType(AreeActions.GET_AREE)
        .pipe(
            switchMap((action: AreeActions.GetAree) => {
                return this.httpClient.get<Area[]>('https://gepre.azurewebsites.net/api/aree/', {
                    observe: 'body',
                    responseType: 'json',
                });
            }), map(
                (aree) => {
                    return {
                        type: AreeActions.SET_AREE,
                        payload: aree,
                    };
                }

            )
        );

    @Effect({ dispatch: false })
    addArea = this.actions$.ofType(AreeActions.ADD_AREA)
        .pipe(
            switchMap((action: AreeActions.AddArea) => {
                console.log(action.payload);

                const req = new HttpRequest('POST', 'https://gepre.azurewebsites.net/api/aree',
                    {
                        'nomeArea': action.payload.nomeArea,
                    }, { reportProgress: true });
                return this.httpClient.request(req);
            }),
        );

    // @Effect({ dispatch: false })
    // deleteDipendente = this.actions$.ofType(AreeActions.DELETE_PRESENZA)
    //     .pipe(
    //         switchMap((action: AreeActions.DeleteArea) => {
    //             const params = 'https://gepre.azurewebsites.net/api/presenze/' + action.payload.toString();
    //             const req = new HttpRequest('DELETE', params,
    //                 { reportProgress: true });
    //             return this.httpClient.request(req);
    //         }),
    //     );

    // @Effect({ dispatch: false })
    // updateDipendente = this.actions$.ofType(AreeActions.UPDATE_PRESENZA)
    //     .pipe(
    //         switchMap((action: AreeActions.UpdateArea) => {

    //             const url = 'https://gepre.azurewebsites.net/api/presenze/' + action.payload;
    //             const req = new HttpRequest('PUT', url,
    //                 {
    //                     'date': action.payload.area.date,
    //                     'stato': action.payload.area.stato,
    //                     'motivoAssenza': action.payload.area.motivoAssenza,
    //                     'dipendenteId': action.payload.area.dipendente.id,

    //                 }, { reportProgress: true });
    //             return this.httpClient.request(req);
    //         })
    //     );

    constructor(private actions$: Actions, private store: Store<fromAree.State>, private httpClient: HttpClient) { }

}
