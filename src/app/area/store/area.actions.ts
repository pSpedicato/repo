import { Action } from '@ngrx/store';
import { Area } from 'src/app/shared/area.model';

export const ADD_AREA = 'ADD_AREA';
export const GET_AREE = 'GET_AREE';
export const SET_AREE = 'SET_AREE';
export const UPDATE_AREA = 'UPDATE_AREA';
export const DELETE_AREA = 'DELETE_AREA';
export const START_EDIT = 'START_EDIT';
export const STOP_EDIT = 'STOP_EDIT';

export class AddArea implements Action {
    readonly type = ADD_AREA;
    constructor(public payload: Area) { }
}
export class GetAree implements Action {
    readonly type = GET_AREE;
}

export class SetAree implements Action {
    readonly type = SET_AREE;
    constructor(public payload: Area[]) { }
}

export type AreeActions = AddArea | GetAree | SetAree;
