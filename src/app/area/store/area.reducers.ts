import * as AreeActions from './area.actions';
import { Area } from 'src/app/shared/area.model';

export interface State {
    aree: Area[];
}

const initialState: State = {
    aree: [],
};

export function areaReducer(state = initialState, action: AreeActions.AreeActions) {
    switch (action.type) {
        case AreeActions.ADD_AREA:
            return {
                ...state,
                aree: [...state.aree, action.payload],
            };

        case AreeActions.SET_AREE:
            return {
                ...state,
                aree: [...action.payload],
            };
        default:
            return state;
    }
}
