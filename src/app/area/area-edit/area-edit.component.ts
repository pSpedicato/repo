import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import * as fromApp from '../../store/app.reducers';
import * as AreeActions from '../store/area.actions';
import { Store } from '@ngrx/store';
import { Area } from 'src/app/shared/area.model';

@Component({
  selector: 'app-area-edit',
  templateUrl: './area-edit.component.html',
  styleUrls: ['./area-edit.component.css']
})
export class AreaEditComponent implements OnInit {
  @ViewChild('f') form: NgForm;
  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
  }

  onSubmit() {
    const nome = this.form.value.nome;
    const area = new Area(null, nome);
    this.store.dispatch(new AreeActions.AddArea(area));
  }
}
