import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/service/auth.service';
import { User } from 'src/app/shared/user.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material';
import { PopupComponent } from '../popup/popup.component';

@Component({
  selector: 'app-change-profile',
  templateUrl: './change-profile.component.html',
  styleUrls: ['./change-profile.component.css']
})
export class ChangeProfileComponent implements OnInit {
  profile: FormGroup;
  user: User;
  constructor(private router: Router,
    private route: ActivatedRoute,
    private authService: AuthService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.user = this.authService.getUser();
    const email = this.user.email;
    const password = this.user.password;
    const confirm = '';
    this.openDialog(this.user);
    this.profile = new FormGroup({
      'email': new FormControl({ value: email, disabled: true }, Validators.required),
      'password': new FormControl(password, Validators.required),
      'confirm': new FormControl(confirm, Validators.required)
    });
  }

  onSubmit() {

    const value = this.profile.value;
    this.user.email = value.email;
    this.user.password = value.password;
    if (value.password === value.confirm) {
      this.authService.changeProfile(this.user);
    }
  }

  onClear() {
    this.profile.reset();
  }

  openDialog(user: User): void {
    this.dialog.open(PopupComponent, {
      width: '50%',
      data: user
    });
  }
}
