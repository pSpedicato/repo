import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/service/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  admin = false;
  auth = false;

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {

    if (sessionStorage.getItem('tk')) {
      this.auth = sessionStorage.getItem('tk') != null;
      this.admin = sessionStorage.getItem('admin') === 'true';
    }
    this.authService.event.subscribe(
      (user) => {
        if (user.user != null) {
          this.admin = user.user.amministratore;
          this.auth = true;
        } else {
          this.admin = false;
          this.auth = false;
        }
      }
    );

  }
  goHome() {
    this.router.navigate(['']);
  }
  onAdmin() {
    this.router.navigate(['admin']);
  }
  onLogout() {
    this.authService.logout();
  }
}
