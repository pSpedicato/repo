import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/service/auth.service';
import { User } from 'src/app/shared/user.model';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {

  user: User;

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  goHome() {
    this.router.navigate(['']);
  }
  onPresenze() {
    this.router.navigate(['presenze']);
  }
  onReportPresenze() {
    this.router.navigate(['presenze', 'report']);
  }
  onAssenze() {
    this.router.navigate(['assenze']);
  }
  onOspiti() {
    this.router.navigate(['ospiti']);
  }
  onDipendenti() {
    this.router.navigate(['dipendenti']);
  }
  admin() {
    this.router.navigate(['admin']);
  }
}
