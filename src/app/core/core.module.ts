import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidenavComponent } from './sidenav/sidenav.component';
import { HeaderComponent } from './header/header.component';
import {
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatSidenavModule,
  MatToolbarModule,
  MatMenuModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatListModule
} from '@angular/material';
import { ChangeProfileComponent } from './change-profile/change-profile.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { PopupComponent } from './popup/popup.component';

@NgModule({
  declarations: [
    SidenavComponent,
    HeaderComponent,
    ChangeProfileComponent,
    PopupComponent,
  ],
  imports: [
    CommonModule,
    MatSidenavModule,
    ReactiveFormsModule,
    MaterialModule,
    MatListModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatButtonToggleModule
  ],
  entryComponents: [
    PopupComponent
  ],
  exports: [
    SidenavComponent,
    HeaderComponent,

  ]
})
export class CoreModule { }
