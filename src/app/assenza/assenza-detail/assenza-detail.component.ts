import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Assenza } from 'src/app/shared/assenza.model';

@Component({
  selector: 'app-assenza-detail',
  templateUrl: './assenza-detail.component.html',
  styleUrls: ['./assenza-detail.component.css']
})
export class AssenzaDetailComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AssenzaDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Assenza) { }

  ngOnInit() {
  }

  onChiudi() {
    this.dialogRef.close();
  }
}
