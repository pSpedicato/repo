import { Action } from '@ngrx/store';
import { Dipendente } from 'src/app/shared/dipendente.model';
import { Assenza } from 'src/app/shared/assenza.model';

export const ADD_ASSENZA = 'ADD_ASSENZA';
export const GET_ASSENZE_DIPENDENTI = 'GET_ASSENZE_DIPENDENTI';
export const GET_ASSENZE_DIPENDENTE = 'GET_ASSENZE_DIPENDENTE';
export const GET_ASSENZE_BADGE = 'GET_ASSENZE_BADGE';
export const GET_ASSENZE_CERCATE = 'GET_ASSENZE_CERCATE';
export const SET_ASSENZE_DIPENDENTI = 'SET_ASSENZE_DIPENDENTI';
export const SET_ASSENZE_OSPITI = 'SET_ASSENZE_OSPITI';
export const SET_ASSENZE_CERCATE = 'SET_ASSENZE_CERCATE';
export const UPDATE_ASSENZA = 'UPDATE_ASSENZA';
export const DELETE_ASSENZA = 'DELETE_ASSENZA';
export const START_EDIT = 'START_EDIT';
export const STOP_EDIT = 'STOP_EDIT';

export class AddAssenza implements Action {
    readonly type = ADD_ASSENZA;
    constructor(public payload: Assenza) { }
}

export class UpdateAssenza implements Action {
    readonly type = UPDATE_ASSENZA;
    constructor(public payload: { assenza: Assenza }) { }
}

export class DeleteAssenza implements Action {
    readonly type = DELETE_ASSENZA;
    constructor(public payload: { assenza: Assenza, riga: number }) { }
}

export class StartEdit implements Action {
    readonly type = START_EDIT;
    constructor(public payload: { id: number }) { }
}

export class StopEdit implements Action {
    readonly type = STOP_EDIT;
}

export class GetAssenzeDipendenti implements Action {
    readonly type = GET_ASSENZE_DIPENDENTI;
}

export class GetAssenzeDipendente implements Action {
    readonly type = GET_ASSENZE_DIPENDENTE;
    constructor(public payload: { id: number }) { }
}

export class GetAssenzeBadge implements Action {
    readonly type = GET_ASSENZE_BADGE;
    constructor(public payload: { badge: number }) { }
}

export class GetAssenzeCercate implements Action {
    readonly type = GET_ASSENZE_CERCATE;
    constructor(public payload: { da: Date, a: Date }) { }
}

export class SetAssenzeDipendenti implements Action {
    readonly type = SET_ASSENZE_DIPENDENTI;
    constructor(public payload: Assenza[]) { }
}

export class SetAssenzeCercate implements Action {
    readonly type = SET_ASSENZE_CERCATE;
    constructor(public payload: Assenza[]) { }
}
export type AssenzeActions = AddAssenza | DeleteAssenza | UpdateAssenza | StartEdit |
    StopEdit | GetAssenzeDipendenti | GetAssenzeBadge | GetAssenzeCercate | SetAssenzeDipendenti |
    SetAssenzeCercate ;
