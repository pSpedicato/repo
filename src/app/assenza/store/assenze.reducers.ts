import * as AssenzeActions from './assenze.actions';
import { Assenza } from 'src/app/shared/assenza.model';

export interface State {
    assenzeAttive: Assenza[];
    assenzeCercate: Assenza[];
    editedAssenza: Assenza;
    editedAssenzaIndex: number;
}

const initialState: State = {
    assenzeAttive: [],
    assenzeCercate: [],
    editedAssenza: null,
    editedAssenzaIndex: -1,
};
export function assenzeReducer(state = initialState, action: AssenzeActions.AssenzeActions) {
    switch (action.type) {
        case AssenzeActions.ADD_ASSENZA:
            return {
                ...state,
                assenzeAttive: [...state.assenzeAttive, action.payload],
            };

        case AssenzeActions.SET_ASSENZE_DIPENDENTI:
            return {
                ...state,
                assenzeAttive: [...action.payload],
            };

        case AssenzeActions.SET_ASSENZE_CERCATE:
            return {
                ...state,
                assenzeCercate: [...action.payload],
            };

        case AssenzeActions.UPDATE_ASSENZA:
            const assenza = state.assenzeAttive[state.editedAssenzaIndex];
            const updatedAssenza = {
                ...assenza,
                ...action.payload.assenza,
            };
            const newAssenze = [...state.assenzeAttive];
            newAssenze[state.editedAssenzaIndex] = updatedAssenza;
            return {
                ...state,
                assenzeAttive: newAssenze,
            };
        case AssenzeActions.DELETE_ASSENZA:
            const oldAssenze = [...state.assenzeAttive];
            oldAssenze.splice(action.payload.riga, 1);
            return {
                ...state,
                assenzeAttive: oldAssenze,
                editedAssenza: null,
                editedAssenzaIndex: -1,
            };

        case AssenzeActions.START_EDIT:
            const editedAssenza = { ...state.assenzeAttive[action.payload.id] };
            return {
                ...state,
                editedAssenza: editedAssenza,
                editedAssenzaIndex: action.payload.id,
            };

        case AssenzeActions.STOP_EDIT:
            return {
                ...state,
                editedAssenza: null,
                editedAssenzaIndex: -1,
            };
        default:
            return state;
    }
}
