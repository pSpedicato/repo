import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Router } from '@angular/router';
import * as AssenzeActions from './assenze.actions';
import * as fromAssenze from './assenze.reducers';
import { Dipendente } from '../../shared/dipendente.model';
import { switchMap, map, withLatestFrom, concat } from 'rxjs/operators';
import { from } from 'rxjs';
import { HttpClient, HttpRequest, HttpResponse, HttpParams } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Assenza } from 'src/app/shared/assenza.model';

@Injectable()
export class AssenzeEffects {
    @Effect()
    getAssenzeAttive = this.actions$.ofType(AssenzeActions.GET_ASSENZE_DIPENDENTI)
        .pipe(
            switchMap((action: AssenzeActions.GetAssenzeDipendenti) => {
                return this.httpClient.get<Assenza[]>('https://gepre.azurewebsites.net/api/assenze/', {
                    observe: 'body',
                    params: {
                        'filter[where][or][0][inizio][gt]': new Date().setHours(0, 0, 0, 0).toString(),
                        'filter[where][or][1][fine][gt]': new Date().setHours(24, 0, 0, 0).toString(),
                        'filter[include]': 'dipendente',
                    },
                    responseType: 'json',
                });
            }), map(
                (assenze) => {
                    return {
                        type: AssenzeActions.SET_ASSENZE_DIPENDENTI,
                        payload: assenze,
                    };
                }
            )
        );

    @Effect()
    getAssenzeDipendente = this.actions$.ofType(AssenzeActions.GET_ASSENZE_DIPENDENTE)
        .pipe(
            switchMap((action: AssenzeActions.GetAssenzeDipendente) => {
                return this.httpClient.get<Assenza[]>(
                    'https://gepre.azurewebsites.net/api/assenze', {
                        observe: 'body',
                        params: {
                            'filter[where][dipendenteId]': action.payload.id.toString(),
                            'filter[include]': 'dipendente',
                            'filter[order]': 'inizio DESC'
                        },
                        responseType: 'json',
                    });
            }), map(
                (assenze) => {
                    return {
                        type: AssenzeActions.SET_ASSENZE_CERCATE,
                        payload: assenze,
                    };
                }
            )
        );

    @Effect()
    getAssenzeCercate = this.actions$.ofType(AssenzeActions.GET_ASSENZE_CERCATE)
        .pipe(
            switchMap((action: AssenzeActions.GetAssenzeCercate) => {
                return this.httpClient.get<Assenza[]>(
                    'https://gepre.azurewebsites.net/api/assenze', {
                        observe: 'body',
                        params: {
                            'filter[where][and][0][inizio][gt]': new Date(action.payload.da).setHours(0, 0, 0, 0).toString(),
                            'filter[where][and][1][inizio][lt]': new Date(action.payload.a).setHours(24, 0, 0, 0).toString(),
                            'filter[order]': 'inizio DESC',
                            'filter[include]': 'dipendente',
                        },
                        responseType: 'json',
                    });
            }), map(
                (assenze) => {
                    return {
                        type: AssenzeActions.SET_ASSENZE_CERCATE,
                        payload: assenze,
                    };
                }
            )
        );

    @Effect()
    getAssenzeBadge = this.actions$.ofType(AssenzeActions.GET_ASSENZE_BADGE)
        .pipe(
            switchMap((action: AssenzeActions.GetAssenzeBadge) => {
                return this.httpClient.get<Dipendente>(
                    'https://gepre.azurewebsites.net/api/dipendenti', {
                        observe: 'body',
                        params: {
                            'filter[where][badge]': action.payload.badge.toString(),
                        },
                        responseType: 'json',
                    });
            }), switchMap(
                (dipendente) => {
                    if (dipendente[0] != null) {
                        return this.httpClient.get<Assenza[]>(
                            'https://gepre.azurewebsites.net/api/assenze', {
                                observe: 'body',
                                params: {
                                    'filter[where][dipendenteId]': dipendente[0].id.toString(),
                                    'filter[order]': 'inizio DESC',
                                    'filter[include]': 'dipendente'
                                },
                                responseType: 'json',
                            });
                    } else {
                        return [];
                    }

                }), map(
                    (assenze) => {
                        if (assenze !== []) {
                            return {
                                type: AssenzeActions.SET_ASSENZE_CERCATE,
                                payload: assenze,
                            };
                        }
                    }
                )
        );

    @Effect({ dispatch: false })
    addAssenze = this.actions$.ofType(AssenzeActions.ADD_ASSENZA)
        .pipe(
            switchMap((action: AssenzeActions.AddAssenza) => {
                const req = new HttpRequest('POST', 'https://gepre.azurewebsites.net/api/assenze',
                    {
                        'tipo': action.payload.tipo,
                        'motivo': action.payload.motivo,
                        'inizio': action.payload.inizio,
                        'fine': action.payload.fine,
                        'luogo': action.payload.luogo,
                        'dipendenteId': action.payload.dipendente.id,
                    }, { reportProgress: true });
                return this.httpClient.request(req);
            }),
        );

    @Effect({ dispatch: false })
    deleteAssenza = this.actions$.ofType(AssenzeActions.DELETE_ASSENZA)
        .pipe(
            switchMap((action: AssenzeActions.DeleteAssenza) => {
                const params = 'https://gepre.azurewebsites.net/api/assenze/' + action.payload.assenza.id.toString();
                const req = new HttpRequest('DELETE', params,
                    { reportProgress: true });
                return this.httpClient.request(req);
            }),
        );

    @Effect({ dispatch: false })
    updateAssenza = this.actions$.ofType(AssenzeActions.UPDATE_ASSENZA)
        .pipe(
            switchMap((action: AssenzeActions.UpdateAssenza) => {
                const url = 'https://gepre.azurewebsites.net/api/assenze/' + action.payload.assenza.id.toString();
                const reqPUT = new HttpRequest('PUT', url,
                    {
                        'inizio': action.payload.assenza.inizio,
                        'fine': action.payload.assenza.fine,
                        'dipendenteId': action.payload.assenza.dipendente.id,
                        'tipo': action.payload.assenza.tipo,
                        'motivo': action.payload.assenza.motivo,
                        'luogo': action.payload.assenza.luogo,
                    }, { reportProgress: true });
                return this.httpClient.request(reqPUT);
            })
        );

    constructor(private actions$: Actions, private store: Store<fromAssenze.State>, private httpClient: HttpClient) { }

}
