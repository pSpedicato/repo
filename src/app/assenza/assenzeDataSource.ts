import { DataSource } from '@angular/cdk/table';

import { BehaviorSubject, Observable, of } from 'rxjs';

import { CollectionViewer } from '@angular/cdk/collections';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducers';
import { Assenza } from 'src/app/shared/assenza.model';


export class AssenzeDataSource implements DataSource<Assenza> {

    private assenzeSubject = new BehaviorSubject<Assenza[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private store: Store<fromApp.AppState>) { }

    //  connect(collectionViewer: CollectionViewer): Observable<Assenza[]> {
    connect(): Observable<Assenza[]> {
        return this.assenzeSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.assenzeSubject.complete();
        this.loadingSubject.complete();
    }

    loadAssenze(ricerca: boolean) {
        this.loadingSubject.next(true);
        if (!ricerca) {
            this.store.select('assenze').subscribe(
                assenze => this.assenzeSubject.next(assenze.assenzeAttive));
        } else {
            this.store.select('assenze').subscribe(
                assenze => { this.assenzeSubject.next(assenze.assenzeCercate);
        });
    }
}
}
