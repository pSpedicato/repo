import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator, MatRowDef, MatDialog } from '@angular/material';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { ExcelService } from 'src/app/presenze/presenze-list/excel.service';
import * as fromApp from '../../store/app.reducers';
import * as AssenzeActions from '../store/assenze.actions';
import { AssenzeDataSource } from '../assenzeDataSource';
import { Assenza } from 'src/app/shared/assenza.model';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { AssenzaDetailComponent } from '../assenza-detail/assenza-detail.component';
import { Dipendente } from 'src/app/shared/dipendente.model';

@Component({
  selector: 'app-assenze-list',
  templateUrl: './assenze-list.component.html',
  styleUrls: ['./assenze-list.component.css'],
  animations: [
    trigger('list', [
      state('in', style({
        opacity: 1,
      })),
      transition('void => *', [
        style({
          opacity: 0,
        }),
        animate(300)
      ]),
      transition('* => void', [
        animate(300, style({
          opacity: 0,
        }))
      ]),
    ]),
  ]
})
export class AssenzeListComponent implements OnInit {

  dataSource: AssenzeDataSource;
  displayedColumns: string[] = ['nome', 'cognome', 'inizio', 'fine', 'tipo', 'motivo', 'star'];
  renderedData: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatRowDef) row: MatRowDef<any>;
  @Input() ricerca: boolean;

  constructor(private store: Store<fromApp.AppState>, private router: Router, private xls: ExcelService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource = new AssenzeDataSource(this.store);
    this.dataSource.loadAssenze(this.ricerca);
    this.dataSource.connect().subscribe(d => this.renderedData = d);
  }
  downloadFile(data: any) {
    const aux = this.renderedData.
      map(
        (el) => {
          const inizio = new Date(el.inizio);
          const fine = new Date(el.fine);
          const timeDiff = Math.abs(fine.getTime() - inizio.getTime());
          const diffHours = Math.floor((timeDiff / (1000 * 60 * 60)) % 24);
          const diffMinutes = Math.floor((timeDiff / (1000 * 60)) % 60);
          const diffDays = Math.floor(timeDiff / (1000 * 3600 * 24));
          const durata = diffDays.toString().concat(' giorni ', diffHours.toString(), ' ore ', diffMinutes.toString(), ' minuti ');
          // tslint:disable-next-line:no-unused-expression
          return ({
            Nome: el.dipendente.nome,
            Cognome: el.dipendente.cognome,
            Badge: el.dipendente.badge,
            TipoAssenza: el.tipo,
            Dal: inizio.toLocaleDateString(),
            oraInizio: inizio.toLocaleTimeString(),
            Al: fine.toLocaleDateString(),
            oraFine: fine.toLocaleTimeString(),
            Durata: durata,
            Luogo: el.luogo,
            Area: el.dipendente.area,
          });
        }
      );
    this.xls.exportAsExcelFile(aux, 'test');
  }

  onDettagli(assenza: Assenza) {
    this.openDialog(assenza);
  }

  onElimina(index: number, assenza: Assenza) {
    this.store.dispatch(new AssenzeActions.DeleteAssenza({ assenza: assenza, riga: index }));
  }
  onModifica(index: number) {
    this.store.dispatch(new AssenzeActions.StartEdit({ id: index }));
  }


  openDialog(assenza: Assenza): void {
    this.dialog.open(AssenzaDetailComponent, {
      width: '50%',
      data: assenza
    });
  }


}
