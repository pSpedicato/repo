import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Dipendente } from 'src/app/shared/dipendente.model';
import { Subscription, Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';
import { debounceTime, distinctUntilChanged, startWith, map } from 'rxjs/operators';
import { Assenza } from 'src/app/shared/assenza.model';
import * as AssenzeActions from '../store/assenze.actions';
import * as fromApp from '../../store/app.reducers';

@Component({
  selector: 'app-assenza-edit',
  templateUrl: './assenza-edit.component.html',
  styleUrls: ['./assenza-edit.component.css']
})
export class AssenzaEditComponent implements OnInit, OnDestroy {

  assenzeForm: FormGroup;
  editMode = false;
  editAssenza: Assenza;
  minDate = new Date();
  idOnDb: number;
  dipendenti: Dipendente[];
  hours = new Array(24);
  minutes = new Array(12);
  subscription: Subscription;
  dipendenteControl = new FormControl('', Validators.required);
  filteredDipendenti: Observable<Dipendente[]>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromApp.AppState>,
    public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.subscription = this.store.select('assenze')
      .subscribe(
        (data) => {
          if (data.editedAssenzaIndex > -1) {
            this.editAssenza = data.editedAssenza;
            this.editMode = true;
          } else {
            this.editMode = false;
          }
          this.init();
        }
      );
    // popolo ore e minuti
    let i;
    for (i = 0; i < 24; i++) {
      this.hours[i] = i;
    }
    let j = 0;
    for (i = 0; i < 12; i++) {
      this.minutes[i] = j;
      j = j + 5;
    }
    this.store.select('dipendenti').subscribe(
      (dipendenti) => {
        this.dipendenti = dipendenti.dipendenti;
      }
    );

    this.filteredDipendenti = this.dipendenteControl.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        startWith<string | Dipendente>(''),
        map(value => typeof value === 'string' ? value : value.badge),
        map(badge => badge ? this._filterDipendenti(badge) : this.dipendenti.slice())
      );


  }


  ngOnDestroy() {
    this.store.dispatch(new AssenzeActions.StopEdit());
    this.subscription.unsubscribe();
  }


  // serve per disaccoppiare il value dalla stringa mostrata nella selezione del dipendente
  displayFn(dipendente?: Dipendente): string | undefined {
    return dipendente ? dipendente.nome.concat(' ', dipendente.cognome, ' - ', dipendente.badge) : undefined;
  }


  init() {
    let inizio = new Date();
    let fine = new Date();
    let oraInizio;
    let oraFine;
    let minutoInizio;
    let minutoFine;
    let tipo;
    let motivo;
    let luogo;

    if (this.editMode) {
      this.dipendenteControl.setValue(this.editAssenza.dipendente);
      inizio = new Date(this.editAssenza.inizio);
      fine = new Date(this.editAssenza.fine);
      oraInizio = inizio.getHours();
      oraFine = fine.getHours();
      minutoInizio = inizio.getMinutes();
      minutoFine = fine.getMinutes();
      tipo = this.editAssenza.tipo;
      motivo = this.editAssenza.motivo;
      luogo = this.editAssenza.luogo;
    }
    this.assenzeForm = new FormGroup({
      'dipendente': this.dipendenteControl,
      'inizio': new FormControl(inizio, Validators.required),
      'oraInizio': new FormControl(oraInizio, Validators.required),
      'minutoInizio': new FormControl(minutoInizio, Validators.required),
      'fine': new FormControl(fine, Validators.required),
      'oraFine': new FormControl(oraFine, Validators.required),
      'minutoFine': new FormControl(minutoFine, Validators.required),
      'tipo': new FormControl(tipo, Validators.required),
      'motivo': new FormControl(motivo, Validators.required),
      'luogo': new FormControl(luogo),
    });

  }

  private _filterDipendenti(value: string): Dipendente[] {
    const filterValue = value.toLowerCase();
    return this.dipendenti.filter(dipendente => dipendente.badge.toLowerCase().indexOf(filterValue) === 0);
  }

  onSubmit() {
    const value = this.assenzeForm.value;
    value.inizio.setHours(value.oraInizio, value.minutoInizio, 0, 0);
    value.fine.setHours(value.oraFine, value.minutoFine, 0, 0);
    const dip: Dipendente = value.dipendente;
    if (dip.nome == null) {
      this.openSnackBar('Dipendente non valido. Clicca sul dipendente desiderato', 'ok');
    } else {
      const assenza = new Assenza(null, value.inizio, dip, value.fine, value.tipo, value.motivo, value.luogo);
      if (this.editMode) {
        assenza.id = this.editAssenza.id;
        this.store.dispatch(new AssenzeActions.UpdateAssenza({ assenza: assenza }));
      } else {
        this.store.dispatch(new AssenzeActions.AddAssenza(assenza));
      }
      this.store.dispatch(new AssenzeActions.StopEdit);
      this.editMode = false;
      this.assenzeForm.reset();
      this.dipendenteControl.setValue('');
    }
  }

  onClear() {
    this.assenzeForm.reset();
    this.dipendenteControl.setValue('');
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
