import { Component, OnInit } from '@angular/core';
import * as fromApp from '../store/app.reducers';
import * as AssenzeActions from './store/assenze.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-assenza',
  templateUrl: './assenza.component.html',
  styleUrls: ['./assenza.component.css']
})
export class AssenzaComponent implements OnInit {

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.store.dispatch(new AssenzeActions.GetAssenzeDipendenti);
  }

}
