import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';

import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AssenzaComponent } from './assenza.component';
import { AssenzaEditComponent } from './assenza-edit/assenza-edit.component';
import { AssenzaHomeComponent } from './assenza-home/assenza-home.component';
import { RouterModule } from '@angular/router';
import { AssenzeListComponent } from './assenze-list/assenze-list.component';
import { AssenzaDetailComponent } from './assenza-detail/assenza-detail.component';

@NgModule({
    declarations: [
        AssenzaComponent,
        AssenzaEditComponent,
        AssenzaHomeComponent,
        AssenzeListComponent,
        AssenzaDetailComponent
    ],
    imports: [
        RouterModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
    ],
    entryComponents: [
        AssenzaDetailComponent
    ],
    exports: [AssenzeListComponent]
})
export class AssenzaModule { }
