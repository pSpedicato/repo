import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { LayoutModule } from '@angular/cdk/layout';
import { HomeComponent } from './home/home.component';
import { PresenzeComponent } from './presenze/presenze.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from './store/app.reducers';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { EffectsModule } from '@ngrx/effects';
import { MaterialModule } from './material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PresenzeEditComponent } from './presenze/presenze-edit/presenze-edit.component';
import { PresenzeModule } from './presenze/presenze.module';
import { dipendenteReducer } from './dipendente/store/dipendente.reducers';
import { DipendenteEffects } from './dipendente/store/dipendente.effects';
import { PresenzeEffects } from './presenze/store/presenze.effects';
import { AreaModule } from './area/area.module';
import { AreaEffects } from './area/store/area.effects';
import { MAT_DATE_LOCALE } from '@angular/material';
import { OspiteEffects } from './ospite/store/ospite.effects';
import { OspiteModule } from './ospite/ospite.module';
import { AssenzaModule } from './assenza/assenza.module';
import { AssenzeEffects } from './assenza/store/assenze.effects';
import { HomeModule } from './home/home.module';
import { AuthGuard } from './auth/service/auth.guard';
import { AuthService } from './auth/service/auth.service';
import { AuthModule } from './auth/auth.module';
import { AdminModule } from './admin/admin.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    CoreModule,
    HomeModule,
    FormsModule,
    ReactiveFormsModule,
    PresenzeModule,
    AssenzaModule,
    AreaModule,
    AdminModule,
    AppRoutingModule,
    LayoutModule,
    AuthModule,
    MaterialModule,
    HttpClientModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([AreaEffects, DipendenteEffects, PresenzeEffects, OspiteEffects, AssenzeEffects]),
  ],
  providers: [
    AuthGuard,
    AuthService,
    { provide: MAT_DATE_LOCALE, useValue: 'it-IT' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
