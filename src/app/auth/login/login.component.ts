import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild('f') form: NgForm;
  error = '';
  constructor(private authService: AuthService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.authService.errorEvent.subscribe((error) => {
      this.openSnackBar(error.error, '');
      console.log(error.error);
    });
  }

  onSignin() {
    const email = this.form.value.email;
    const password = this.form.value.password;
    this.authService.singinUser(email, password);
    this.authService.event.subscribe(
      (params) => {
        this.error = params['message'];
      }
    );
  }
  onSignout() {
    this.authService.logout();
  }

  openSnackBar(message: string, action: string) {

    this.snackBar.open(message, action, {
      duration: 3000,
      panelClass: ['custom-class'],
    });
  }
}
