import { Injectable, EventEmitter } from '@angular/core';
import { Params, Router } from '@angular/router';
import { HttpClient, HttpRequest, HttpEventType } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from 'src/app/shared/user.model';

declare var Object: any;
export class Resp {
    id: string;
    userId: string;
}
@Injectable()
export class AuthService {
    tk: string;
    userId: string;
    currentUser: User;
    admin: boolean;
    error: Params;
    constructor(private router: Router, private httpClient: HttpClient) { }
    event = new EventEmitter<Params>();
    errorEvent = new EventEmitter<Params>();
    finitoRegistrazione = new EventEmitter<Params>();
    isAuthenticated() {
        return this.tk != null || sessionStorage.getItem('tk') != null;
    }

    getUser() {
        return this.currentUser;
    }
    singinUser(email: string, password: string) {
        const req = new HttpRequest('POST', 'https://gepre.azurewebsites.net/api/users/login?include=user',
            {
                'email': email,
                'password': password
            }, {
                responseType: 'json',
                reportProgress: true
            });

        return this.httpClient.request(req).subscribe(
            (res) => {
                if (res.type === HttpEventType.Response) {
                    const body = res.clone().body;
                    if (body != null) {
                        this.tk = body['id'];
                        this.admin = body['user'].amministratore;
                        this.userId = body['userId'];
                        this.currentUser = body['user'];
                        if (body['user'].firstAccess) {
                            this.router.navigate(['changeProfile']);
                        } else {
                            sessionStorage.setItem('tk', this.tk);
                            sessionStorage.setItem('userId', this.userId);
                            sessionStorage.setItem('admin', this.admin.toString());
                            sessionStorage.setItem('username', this.currentUser.nome);
                            this.router.navigate(['']);
                            this.event.emit({ user: this.currentUser });
                        }
                    }
                }
            },
            (error) => {
                this.errorEvent.emit({ error: 'Login fallito. Controlla email e password' });
            }
        );
    }

    changeProfile(user: User) {
        const req = new HttpRequest('PUT', 'https://gepre.azurewebsites.net/api/users/'
            + user.id.toString() + '?access_token=' + this.tk,
            {
                'password': user.password,
                'firstAccess': false
            }, {
                responseType: 'json',
                reportProgress: true
            });

        return this.httpClient.request(req).subscribe(
            (res) => {
                if (res.type === HttpEventType.Response) {
                    const body = res.clone().body;
                    if (body != null) {
                        sessionStorage.setItem('tk', this.tk);
                        sessionStorage.setItem('userId', this.userId);
                        sessionStorage.setItem('admin', this.admin.toString());
                        sessionStorage.setItem('username', this.currentUser.nome);
                        this.router.navigate(['']);
                        this.event.emit({ user: this.currentUser });
                    }
                }
            }
        );
    }
    signupUser(user: User) {
        const req = new HttpRequest('POST', 'https://gepre.azurewebsites.net/api/users/',
            {
                'nome': user.nome,
                'cognome': user.cognome,
                'firstAccess': true,
                'amministratore': user.amministratore,
                'email': user.email,
                'password': user.password,
            }, {
                responseType: 'json',
                reportProgress: true
            });

        return this.httpClient.request(req).subscribe(
            (res) => {
                if (res.type === HttpEventType.Response) {
                    const body = res.clone().body;
                    if (body != null) {
                        this.finitoRegistrazione.emit({
                            nome: user.nome,
                            email: user.email,
                        });
                    }
                }
            },
            (error) => {
                if (error['error'].error.details.messages.email[0] === 'Email already exists') {
                    this.errorEvent.emit({ error: 'Email già registrata' });
                }
            }
        );
    }
    getToken() {
        return sessionStorage.getItem('tk');
    }
    logout() {
        const req = new HttpRequest('POST', 'https://gepre.azurewebsites.net/api/users/logout?access_token=' + this.getToken(), {});
        this.tk = null;
        this.currentUser = null;
        sessionStorage.clear();
        this.userId = null;
        this.admin = null;
        this.event.emit({ user: this.currentUser });
        this.router.navigate(['login']);
        return this.httpClient.request(req);
    }

}
