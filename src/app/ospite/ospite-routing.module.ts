import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OspiteComponent } from './ospite.component';
import { OspiteListComponent } from './ospite-list/ospite-list.component';
import { OspiteEditComponent } from './ospite-edit/ospite-edit.component';
import { OspiteDetailComponent } from './ospite-detail/ospite-detail.component';
import { OspiteHomeComponent } from './ospite-home/ospite-home.component';

const ospiteRoutes: Routes = [
    {
        path: '', component: OspiteComponent, children: [
            { path: '', component: OspiteHomeComponent },
            { path: 'registrati', component: OspiteListComponent },
            { path: 'new', component: OspiteEditComponent },
            { path: ':id', component: OspiteDetailComponent },
            { path: ':id/edit', component: OspiteEditComponent },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(ospiteRoutes)
    ],
    exports: [RouterModule],
    providers: [
        //      AuthGuard
    ]
})
export class OspiteRoutingModule { }
