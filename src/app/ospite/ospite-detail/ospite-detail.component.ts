import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import * as OspiteActions from '../store/ospite.actions';
import * as PresenzeActions from '../../presenze/store/presenze.actions';
import * as fromOspiti from '../store/ospite.reducers';
import * as fromApp from '../../store/app.reducers';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Store } from '@ngrx/store';
import { Ospite } from 'src/app/shared/ospite.model';

@Component({
  selector: 'app-ospite-detail',
  templateUrl: './ospite-detail.component.html',
  styleUrls: ['./ospite-detail.component.css']
})
export class OspiteDetailComponent implements OnInit {
  index: number;
  ospite: Ospite;
  ospitiState: Observable<fromOspiti.State>;
  idDb: number;
  constructor(private route: ActivatedRoute,
    private store: Store<fromApp.AppState>,
    private router: Router) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.index = +params['id'];
          this.ospitiState = this.store.select('ospiti');
        }
      );
    this.ospitiState.subscribe(
      (ospiti) => {
        this.ospite = ospiti.ospiti[this.index];
      }
    );
    this.store.dispatch(new PresenzeActions.GetPresenzePersona({ id: this.ospite.id, isOspite: true }));
  }
  onTorna() {
    this.router.navigate(['/ospiti']);
  }

  onModifica() {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

}
