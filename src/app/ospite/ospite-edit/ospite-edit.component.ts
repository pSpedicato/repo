import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Ospite } from 'src/app/shared/ospite.model';
import { Store } from '@ngrx/store';
import * as OspiteActions from '../store/ospite.actions';
import * as fromOspiti from '../store/ospite.reducers';
import * as fromApp from '../../store/app.reducers';
import { Router, Route, ActivatedRoute, Params } from '@angular/router';
import { take } from 'rxjs/operators';
import { ReadPropExpr } from '@angular/compiler';

@Component({
  selector: 'app-ospite-edit',
  templateUrl: './ospite-edit.component.html',
  styleUrls: ['./ospite-edit.component.css']
})
export class OspiteEditComponent implements OnInit {
  ospiteForm: FormGroup;
  id: number;
  editMode = false;
  idOnDb: number;
  constructor(private store: Store<fromApp.AppState>, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
        this.init();
      }
    );

  }

  init() {
    let nome = '';
    let cognome = '';
    let documentoId = '';
    let societa = '';
    if (this.editMode) {
      this.store.select('ospiti')
        .pipe(take(1))
        .subscribe((ospitiState: fromOspiti.State) => {
          const ospite = ospitiState.ospiti[this.id];
          this.idOnDb = ospite.id;
          nome = ospite.nome;
          cognome = ospite.cognome;
          documentoId = ospite.documentoID;
          societa = ospite.societa;
        });
    }
    this.ospiteForm = new FormGroup({
      'nome': new FormControl(nome, Validators.required),
      'cognome': new FormControl(cognome, Validators.required),
      'documentoId': new FormControl(documentoId, Validators.required),
      'societa': new FormControl(societa),
    });
  }

  onSubmit() {
    const value = this.ospiteForm.value;
    const ospite = new Ospite(
      this.idOnDb,
      value.nome,
      value.cognome,
      value.documentoId,
      value.societa);
    if (this.editMode) {
      this.store.dispatch(new OspiteActions.UpdateOspite({ index: this.id, idOnDb: ospite.id, ospite: ospite }));
    } else {
      this.store.dispatch(new OspiteActions.AddOspite(ospite));
    }
    // ospiti non caricati immediatamente
    this.router.navigate(['ospiti']);
  }

  onClear() {
    this.ospiteForm.reset();
  }
  onBack() {
    this.router.navigate(['ospiti']);
  }
}
