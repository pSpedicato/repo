import { Component, OnInit, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import * as fromApp from '../store/app.reducers';
import * as OspiteActions from './store/ospite.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-ospite',
  templateUrl: './ospite.component.html',
  styleUrls: ['./ospite.component.css']
})
export class OspiteComponent implements OnInit, OnChanges {

  constructor(private store: Store<fromApp.AppState>, private router: Router) { }

  ngOnInit() {
    this.store.dispatch(new OspiteActions.GetOspiti);
  }
  ngOnChanges() {
    this.store.dispatch(new OspiteActions.GetOspiti);
  }
  onAddOspite() {
    this.router.navigate(['ospiti', 'new']);
  }
  onIndietro() {
    this.router.navigate(['ospiti']);
  }

}
