import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducers';
import * as OspiteActions from '../store/ospite.actions';
import { Observable } from 'rxjs';
import { MatTableDataSource, MatPaginator, MatRow, MatRowDef } from '@angular/material';
import { Ospite } from 'src/app/shared/ospite.model';
import { OspiteDataSource } from './ospiteDataSource';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ospite-list',
  templateUrl: './ospite-list.component.html',
  styleUrls: ['./ospite-list.component.css']
})
export class OspiteListComponent implements OnInit {
  dataSource: OspiteDataSource;
  displayedColumns: string[] = ['nome', 'cognome', 'documentoId', 'societa', 'star'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatRowDef) row: MatRowDef<any>;

  constructor(private store: Store<fromApp.AppState>, private router: Router) { }

  ngOnInit() {
    this.dataSource = new OspiteDataSource(this.store);
    this.dataSource.loadOspiti();
  }

  onRowClicked(index: number) {
    this.router.navigate(['ospiti', index]);
  }

  onDettagli(index: number) {
    this.router.navigate(['ospiti', index]);
  }

  onElimina(index: number, indexOnDb: number) {
    this.store.dispatch(new OspiteActions.DeleteOspite({ id: index, idOnDb: indexOnDb }));
  }

  onModifica(index: number) {
    this.router.navigate(['ospiti', index, 'edit']);
  }
}
