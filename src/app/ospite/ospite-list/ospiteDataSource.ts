import { DataSource } from '@angular/cdk/table';

import { BehaviorSubject, Observable, of } from 'rxjs';

import { CollectionViewer } from '@angular/cdk/collections';

import { catchError, finalize } from 'rxjs/operators';
import { Ospite } from 'src/app/shared/ospite.model';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducers';


export class OspiteDataSource implements DataSource<Ospite> {

    private ospiteSubject = new BehaviorSubject<Ospite[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private store: Store<fromApp.AppState>) { }

    connect(collectionViewer: CollectionViewer): Observable<Ospite[]> {
        return this.ospiteSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.ospiteSubject.complete();
        this.loadingSubject.complete();
    }

    loadOspiti() {
        this.loadingSubject.next(true);
        this.store.select('ospiti').subscribe(
            ospiti => this.ospiteSubject.next(ospiti.ospiti));

    }
}
