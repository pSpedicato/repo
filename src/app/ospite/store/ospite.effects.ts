
import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Router } from '@angular/router';
import * as OspiteActions from './ospite.actions';
import * as fromOspite from './ospite.reducers';
import { Ospite } from '../../shared/ospite.model';
import { switchMap, map, withLatestFrom } from 'rxjs/operators';
import { from } from 'rxjs';
import { HttpClient, HttpRequest, HttpResponse, HttpParams } from '@angular/common/http';
import { Store } from '@ngrx/store';

@Injectable()
export class OspiteEffects {
    @Effect()
    getOspiti = this.actions$.ofType(OspiteActions.GET_OSPITI)
        .pipe(
            switchMap((action: OspiteActions.GetOspiti) => {
                return this.httpClient.get<Ospite[]>('https://gepre.azurewebsites.net/api/ospiti', {
                    observe: 'body',
                    responseType: 'json'
                });
            }), map(
                (ospiti) => {
                    return {
                        type: OspiteActions.SET_OSPITI,
                        payload: ospiti,
                    };
                }
            )
        );

    @Effect({ dispatch: false })
    addOspite = this.actions$.ofType(OspiteActions.ADD_OSPITE)
        .pipe(
            switchMap((action: OspiteActions.AddOspite) => {
                const req = new HttpRequest('POST', 'https://gepre.azurewebsites.net/api/ospiti',
                    {
                        'nome': action.payload.nome,
                        'cognome': action.payload.cognome,
                        'documentoID': action.payload.documentoID,
                        'societa': action.payload.societa,
                    }, { reportProgress: true });
                return this.httpClient.request(req);
            }),
            map((response) => {
                if (response['status'] === 200) {
                    this.store.dispatch(new OspiteActions.GetOspiti);
                }
            })
        );

    @Effect({ dispatch: false })
    deleteOspite = this.actions$.ofType(OspiteActions.DELETE_OSPITE)
        .pipe(
            switchMap((action: OspiteActions.DeleteOspite) => {
                const params = 'https://gepre.azurewebsites.net/api/ospiti/' + action.payload.idOnDb.toString();
                const req = new HttpRequest('DELETE', params,
                    { reportProgress: true });
                return this.httpClient.request(req);
            }),
        );

    @Effect({ dispatch: false })
    updateOspite = this.actions$.ofType(OspiteActions.UPDATE_OSPITE)
        .pipe(
            switchMap((action: OspiteActions.UpdateOspite) => {

                const url = 'https://gepre.azurewebsites.net/api/ospiti/' + action.payload.idOnDb;
                const req = new HttpRequest('PUT', url,
                    {
                        'nome': action.payload.ospite.nome,
                        'cognome': action.payload.ospite.cognome,
                        'documentoId': action.payload.ospite.documentoID,
                        'societa': action.payload.ospite.societa,
                    }, { reportProgress: true });
                return this.httpClient.request(req);
            })
        );

    constructor(private actions$: Actions, private store: Store<fromOspite.State>, private httpClient: HttpClient) { }

}
