import * as OspiteActions from './ospite.actions';
import { Ospite } from 'src/app/shared/ospite.model';

export interface State {
    ospiti: Ospite[];
}

const initialState: State = {
    ospiti: [],
};
export function opsiteReducer(state = initialState, action: OspiteActions.OspiteActions) {
    switch (action.type) {
        case (OspiteActions.SET_OSPITI):
            return {
                ...state,
                ospiti: [...action.payload],
            };
        case OspiteActions.ADD_OSPITE:
            return {
                ...state,
                ospiti: [...state.ospiti, action.payload],
            };
        case OspiteActions.UPDATE_OSPITE:
            const ospite = state.ospiti[action.payload.index];
            const updatedOspite = {
                ...ospite,
                ...action.payload.ospite,
            };
            const newOspiti = [...state.ospiti];
            newOspiti[action.payload.index] = updatedOspite;
            return {
                ...state,
                ospiti: newOspiti,
            };
        case OspiteActions.DELETE_OSPITE:
            const updatedOspiti = [...state.ospiti];
            updatedOspiti.splice(action.payload.id, 1);
            return {
                ...state,
                ospiti: updatedOspiti
            };

        default:
            return state;
    }
}
