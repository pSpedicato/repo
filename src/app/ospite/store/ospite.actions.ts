import { Action } from '@ngrx/store';
import { Ospite } from 'src/app/shared/ospite.model';

export const ADD_OSPITE = 'ADD_OSPITE';
export const UPDATE_OSPITE = 'UPDATE_OSPITE';
export const DELETE_OSPITE = 'DELETE_OSPITE';
export const GET_OSPITI = 'GET_OSPITI';
export const SET_OSPITI = 'SET_OSPITI';

export class AddOspite implements Action {
    readonly type = ADD_OSPITE;
    constructor(public payload: Ospite) { }
}

export class UpdateOspite implements Action {
    readonly type = UPDATE_OSPITE;
    constructor(public payload: { index: number, idOnDb: number, ospite: Ospite }) { }
}

export class DeleteOspite implements Action {
    readonly type = DELETE_OSPITE;
    constructor(public payload: { id: number, idOnDb: number }) { }
}

export class GetOspiti implements Action {
    readonly type = GET_OSPITI;
}

export class SetOspiti implements Action {
    readonly type = SET_OSPITI;
    constructor(public payload: Ospite[]) { }
}

export type OspiteActions = AddOspite | DeleteOspite | UpdateOspite | GetOspiti | SetOspiti;
