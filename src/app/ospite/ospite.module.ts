import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';

import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OspiteComponent } from './ospite.component';
import { OspiteEditComponent } from './ospite-edit/ospite-edit.component';
import { OspiteListComponent } from './ospite-list/ospite-list.component';
import { OspiteRoutingModule } from './ospite-routing.module';
import { OspiteDetailComponent } from './ospite-detail/ospite-detail.component';
import { PresenzeModule } from '../presenze/presenze.module';
import { OspiteHomeComponent } from './ospite-home/ospite-home.component';

@NgModule({
  declarations: [
    OspiteComponent,
    OspiteEditComponent,
    OspiteDetailComponent,
    OspiteListComponent,
    OspiteHomeComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    OspiteRoutingModule,
    PresenzeModule,
  ]
})
export class OspiteModule { }
