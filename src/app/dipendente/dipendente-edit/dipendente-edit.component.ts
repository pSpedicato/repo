import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Dipendente } from 'src/app/shared/dipendente.model';
import { Store } from '@ngrx/store';
import * as DipendenteActions from '../store/dipendente.actions';
import * as fromDipendenti from '../store/dipendente.reducers';
import * as fromApp from '../../store/app.reducers';
import * as AreeActions from '../../area/store/area.actions';
import { Router, Route, ActivatedRoute, Params } from '@angular/router';
import { take } from 'rxjs/operators';
import { ReadPropExpr } from '@angular/compiler';
import { Area } from 'src/app/shared/area.model';

@Component({
  selector: 'app-dipendente-edit',
  templateUrl: './dipendente-edit.component.html',
  styleUrls: ['./dipendente-edit.component.css']
})
export class DipendenteEditComponent implements OnInit {
  dipenendenteForm: FormGroup;
  id: number;
  editMode = false;
  idOnDb: number;
  aree: Area[];
  areaScelta: number;
  constructor(private store: Store<fromApp.AppState>, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.store.dispatch(new AreeActions.GetAree);
    this.store.select('aree').subscribe(
      (aree) => {
        this.aree = aree.aree;
      }
    );
    this.route.params.subscribe(
      (params: Params) => {
        this.id = +params['id'];
        this.editMode = params['id'] != null;
        this.init();
      }
    );

  }

  init() {
    let badge = '';
    let nome = '';
    let cognome = '';
    let profiloProfessionale = '';
    let funzAziendale = '';
    let sede = '';
    let rapporto = '';
    let area;
    let societa = '';
    if (this.editMode) {
      this.store.select('dipendenti')
        .pipe(take(1))
        .subscribe((dipendentiState: fromDipendenti.State) => {
          const dipendente = dipendentiState.dipendenti[this.id];
          this.idOnDb = dipendente.id;
          badge = dipendente.badge;
          nome = dipendente.nome;
          cognome = dipendente.cognome;
          profiloProfessionale = dipendente.profiloProfessionale;
          funzAziendale = dipendente.funzAziendale;
          sede = dipendente.sede;
          rapporto = dipendente.rapporto;
          area = dipendente.area.id - 1;
          societa = dipendente.societa;
        });
    }
    this.dipenendenteForm = new FormGroup({
      'badge': new FormControl(badge, Validators.required),
      'nome': new FormControl(nome, Validators.required),
      'cognome': new FormControl(cognome, Validators.required),
      'profiloProfessionale': new FormControl(profiloProfessionale, Validators.required),
      'funzAziendale': new FormControl(funzAziendale, Validators.required),
      'sede': new FormControl(sede, Validators.required),
      'rapporto': new FormControl(rapporto, Validators.required),
      'area': new FormControl(area, Validators.required),
      'societa': new FormControl(societa),
    });
  }

  onSubmit() {
    const value = this.dipenendenteForm.value;
    const dipendente = new Dipendente(
      this.idOnDb,
      value.nome,
      value.cognome,
      value.badge,
      value.profiloProfessionale,
      value.funzAziendale,
      value.sede,
      value.rapporto,
      this.aree[value.area],
      value.societa);
    if (this.editMode) {
      this.store.dispatch(new DipendenteActions.UpdateDipendente({ index: this.id, idOnDb: dipendente.id, dipendente: dipendente }));
    } else {
      this.store.dispatch(new DipendenteActions.AddDipendente(dipendente));
    }
    this.router.navigate(['dipendenti']);
  }

  onClear() {
    this.dipenendenteForm.reset();
  }
  onBack() {
    this.router.navigate(['dipendenti']);
  }
}
