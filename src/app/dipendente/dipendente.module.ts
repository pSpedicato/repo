import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';

import { CommonModule } from '@angular/common';
import { DipendenteComponent } from './dipendente.component';
import { DipendenteEditComponent } from './dipendente-edit/dipendente-edit.component';
import { DipendenteListComponent } from './dipendente-list/dipendente-list.component';
import { DipendenteDetailComponent } from './dipendente-detail/dipendente-detail.component';
import { dipendenteReducer } from './store/dipendente.reducers';
import { EffectsModule } from '@ngrx/effects';
import { DipendenteEffects } from './store/dipendente.effects';
import { DipendenteRoutingModule } from './dipendente-routing.module';
import { MaterialModule } from '../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DipendentePresenzaComponent } from './dipendente-detail/dipendente-presenza/dipendente-presenza.component';
import { AssenzaModule } from '../assenza/assenza.module';
import { PresenzeModule } from '../presenze/presenze.module';

@NgModule({
  declarations: [
    DipendenteComponent,
    DipendenteEditComponent,
    DipendenteListComponent,
    DipendenteDetailComponent,
    DipendentePresenzaComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AssenzaModule,
    PresenzeModule,
    MaterialModule,
    // StoreModule.forFeature('dipendenti', dipendenteReducer),
    // EffectsModule.forFeature([DipendenteEffects]),
    DipendenteRoutingModule,

  ]
})
export class DipendenteModule { }
