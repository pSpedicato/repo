import { Action } from '@ngrx/store';
import { Dipendente } from 'src/app/shared/dipendente.model';

export const ADD_DIPENDENTE = 'ADD_DIPENDENTE';
export const ADD_DIPENDENTI = 'ADD_DIPENDENTI';
export const UPDATE_DIPENDENTE = 'UPDATE_DIPENDENTE';
export const DELETE_DIPENDENTE = 'DELETE_DIPENDENTE';
export const GET_DIPENDENTI = 'GET_DIPENDENTI';
export const SET_DIPENDENTI = 'SET_DIPENDENTI';
// export const ADD_DIPENDENTE_FINISHED = 'ADD_DIPENDENTE_FINISHED';

export class AddDipendente implements Action {
    readonly type = ADD_DIPENDENTE;
    constructor(public payload: Dipendente) { }
}

export class AddDipendenti implements Action {
    readonly type = ADD_DIPENDENTI;
    constructor(public payload: any[]) { }
}


export class UpdateDipendente implements Action {
    readonly type = UPDATE_DIPENDENTE;
    constructor(public payload: { index: number, idOnDb: number, dipendente: Dipendente }) { }
}

export class DeleteDipendente implements Action {
    readonly type = DELETE_DIPENDENTE;
    constructor(public payload: { id: number, idOnDb: number }) { }
}

export class GetDipendenti implements Action {
    readonly type = GET_DIPENDENTI;
}
// export class AddDipendenteFinished implements Action {
//     readonly type = ADD_DIPENDENTE_FINISHED;
//     constructor(public payload: { dipendente: Dipendente }) { }
// }

export class SetDipendenti implements Action {
    readonly type = SET_DIPENDENTI;
    constructor(public payload: Dipendente[]) { }
}

export type DipendenteActions = AddDipendente | AddDipendenti | DeleteDipendente | UpdateDipendente | GetDipendenti | SetDipendenti;
