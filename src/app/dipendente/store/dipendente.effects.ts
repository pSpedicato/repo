import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Router } from '@angular/router';
import * as DipendenteActions from './dipendente.actions';
import * as fromDipendente from './dipendente.reducers';
import { Dipendente } from '../../shared/dipendente.model';
import { switchMap, map, withLatestFrom } from 'rxjs/operators';
import { from } from 'rxjs';
import { HttpClient, HttpRequest, HttpResponse, HttpParams } from '@angular/common/http';
import { Store } from '@ngrx/store';

@Injectable()
export class DipendenteEffects {
    @Effect()
    getDipendenti = this.actions$.ofType(DipendenteActions.GET_DIPENDENTI)
        .pipe(
            switchMap((action: DipendenteActions.GetDipendenti) => {
                return this.httpClient.get<Dipendente[]>('https://gepre.azurewebsites.net/api/dipendenti?filter[include]=area', {
                    observe: 'body',
                    responseType: 'json'
                });
            }), map(
                (dipendenti) => {
                    return {
                        type: DipendenteActions.SET_DIPENDENTI,
                        payload: dipendenti,
                    };
                }
            )
        );

    @Effect({ dispatch: false })
    addDipendente = this.actions$.ofType(DipendenteActions.ADD_DIPENDENTE)
        .pipe(
            switchMap((action: DipendenteActions.AddDipendente) => {
                const req = new HttpRequest('POST', 'https://gepre.azurewebsites.net/api/dipendenti',
                    {
                        'nome': action.payload.nome,
                        'cognome': action.payload.cognome,
                        'profiloProfessionale': action.payload.profiloProfessionale,
                        'funzAziendale': action.payload.funzAziendale,
                        'sede': action.payload.sede,
                        'rapporto': action.payload.rapporto,
                        'areaId': action.payload.area.id,
                        'societa': action.payload.societa,
                        'badge': action.payload.badge,
                    }, { reportProgress: true });
                return this.httpClient.request(req);
            }),
            map((response) => {
                if (response['status'] === 200) {
                    this.store.dispatch(new DipendenteActions.GetDipendenti);
                }
            })
        );

    @Effect({ dispatch: false })
    deleteDipendente = this.actions$.ofType(DipendenteActions.DELETE_DIPENDENTE)
        .pipe(
            switchMap((action: DipendenteActions.DeleteDipendente) => {
                const params = 'https://gepre.azurewebsites.net/api/dipendenti/' + action.payload.idOnDb.toString();
                const req = new HttpRequest('DELETE', params,
                    { reportProgress: true });
                return this.httpClient.request(req);
            }),
        );

    @Effect({ dispatch: false })
    updateDipendente = this.actions$.ofType(DipendenteActions.UPDATE_DIPENDENTE)
        .pipe(
            switchMap((action: DipendenteActions.UpdateDipendente) => {
                const url = 'https://gepre.azurewebsites.net/api/dipendenti/' + action.payload.idOnDb;
                const req = new HttpRequest('PUT', url,
                    {
                        'nome': action.payload.dipendente.nome,
                        'cognome': action.payload.dipendente.cognome,
                        'profiloProfessionale': action.payload.dipendente.profiloProfessionale,
                        'funzAziendale': action.payload.dipendente.funzAziendale,
                        'sede': action.payload.dipendente.sede,
                        'rapporto': action.payload.dipendente.rapporto,
                        'areaId': action.payload.dipendente.area.id,
                        'societa': action.payload.dipendente.societa,
                        'badge': action.payload.dipendente.badge,
                    }, { reportProgress: true });
                return this.httpClient.request(req);
            }),
            map((response) => {
                if (response['status'] === 200) {
                    this.store.dispatch(new DipendenteActions.GetDipendenti);
                }
            })
        );

    constructor(private actions$: Actions, private store: Store<fromDipendente.State>, private httpClient: HttpClient) { }

}
