import * as DipendenteActions from './dipendente.actions';
import { Dipendente } from 'src/app/shared/dipendente.model';

export interface State {
    dipendenti: Dipendente[];
}

const initialState: State = {
    dipendenti: [],
};
export function dipendenteReducer(state = initialState, action: DipendenteActions.DipendenteActions) {
    switch (action.type) {
        case (DipendenteActions.SET_DIPENDENTI):
            return {
                ...state,
                dipendenti: [...action.payload],
            };
        case DipendenteActions.ADD_DIPENDENTE:
            return {
                ...state,
                dipendenti: [...state.dipendenti, action.payload],
            };
        case DipendenteActions.UPDATE_DIPENDENTE:
            const dipendente = state.dipendenti[action.payload.index];
            const updatedDipendente = {
                ...dipendente,
                ...action.payload.dipendente,
            };
            const newDipendenti = [...state.dipendenti];
            newDipendenti[action.payload.index] = updatedDipendente;
            return {
                ...state,
                dipendenti: newDipendenti,
            };
        case DipendenteActions.DELETE_DIPENDENTE:
            const updatedDipendenti = [...state.dipendenti];
            updatedDipendenti.splice(action.payload.id, 1);
            return {
                ...state,
                dipendenti: updatedDipendenti
            };

        default:
            return state;
    }
}
