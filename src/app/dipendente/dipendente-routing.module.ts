import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DipendenteComponent } from './dipendente.component';
import { DipendenteEditComponent } from './dipendente-edit/dipendente-edit.component';
import { DipendenteDetailComponent } from './dipendente-detail/dipendente-detail.component';
import { DipendenteListComponent } from './dipendente-list/dipendente-list.component';

const dipendenteRoutes: Routes = [
    {
        path: '', component: DipendenteComponent, children: [
            { path: '', component: DipendenteListComponent },
            { path: 'new', component: DipendenteEditComponent },
            { path: ':id', component: DipendenteDetailComponent },
            { path: ':id/edit', component: DipendenteEditComponent },
        ]
    },
];

@NgModule({
    imports: [
        RouterModule.forChild(dipendenteRoutes)
    ],
    exports: [RouterModule],
    providers: [
        //      AuthGuard
    ]
})
export class DipendenteRoutingModule { }
