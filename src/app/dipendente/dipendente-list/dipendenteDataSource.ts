import { DataSource } from '@angular/cdk/table';

import { BehaviorSubject, Observable, of } from 'rxjs';

import { CollectionViewer } from '@angular/cdk/collections';

import { catchError, finalize } from 'rxjs/operators';
import { Dipendente } from 'src/app/shared/dipendente.model';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducers';


export class DipendenteDataSource implements DataSource<Dipendente> {

    private dipendenteSubject = new BehaviorSubject<Dipendente[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private store: Store<fromApp.AppState>) { }

    connect(collectionViewer: CollectionViewer): Observable<Dipendente[]> {
        return this.dipendenteSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.dipendenteSubject.complete();
        this.loadingSubject.complete();
    }

    loadDipendenti() {
        this.loadingSubject.next(true);
        this.store.select('dipendenti').subscribe(
            dipendenti => this.dipendenteSubject.next(dipendenti.dipendenti));

    }
}
