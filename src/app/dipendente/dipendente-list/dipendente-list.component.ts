import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducers';
import * as DipendenteActions from '../store/dipendente.actions';
import * as fromDipendenti from '../store/dipendente.reducers';
import { Observable } from 'rxjs';
import { MatTableDataSource, MatPaginator, MatRow, MatRowDef } from '@angular/material';
import { Dipendente } from 'src/app/shared/dipendente.model';
import { DipendenteDataSource } from './dipendenteDataSource';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dipendente-list',
  templateUrl: './dipendente-list.component.html',
  styleUrls: ['./dipendente-list.component.css']
})
export class DipendenteListComponent implements OnInit {
  dataSource: DipendenteDataSource;
  displayedColumns: string[] = ['nome', 'cognome', 'badge', 'star'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatRowDef) row: MatRowDef<any>;

  constructor(private store: Store<fromApp.AppState>, private router: Router) { }

  ngOnInit() {
    this.dataSource = new DipendenteDataSource(this.store);
    this.dataSource.loadDipendenti();
  }

  onRowClicked(index: number) {
    this.router.navigate(['dipendenti', index]);
  }

  onDettagli(index: number) {
    this.router.navigate(['dipendenti', index]);
  }

  onElimina(index: number, indexOnDb: number) {
    this.store.dispatch(new DipendenteActions.DeleteDipendente({ id: index, idOnDb: indexOnDb }));
  }
  onAggiungiDipendente() {
    this.router.navigate(['dipendenti', 'new']);
  }
  onModifica(index: number) {
    this.router.navigate(['dipendenti', index, 'edit']);
  }
}
