import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import * as DipendenteActions from '../store/dipendente.actions';
import * as AssenzeActions from '../../assenza/store/assenze.actions';
import * as PresenzeActions from '../../presenze/store/presenze.actions';
import * as fromDipendenti from '../store/dipendente.reducers';
import * as fromApp from '../../store/app.reducers';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Store } from '@ngrx/store';
import { Dipendente } from 'src/app/shared/dipendente.model';

@Component({
  selector: 'app-dipendente-detail',
  templateUrl: './dipendente-detail.component.html',
  styleUrls: ['./dipendente-detail.component.css']
})
export class DipendenteDetailComponent implements OnInit {
  index: number;
  dipendentiState: Observable<fromDipendenti.State>;
  dipendente: Dipendente;
  idDb: number;
  constructor(private route: ActivatedRoute,
    private store: Store<fromApp.AppState>,
    private router: Router) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.index = +params['id'];
          this.dipendentiState = this.store.select('dipendenti');
          this.dipendentiState.subscribe((dipendenti) => {
            this.dipendente = dipendenti.dipendenti[this.index];
          });
        }
      );
    this.store.dispatch(new AssenzeActions.GetAssenzeDipendente({ id: this.dipendente.id }));
    this.store.dispatch(new PresenzeActions.GetPresenzePersona({ id: this.dipendente.id, isOspite: false }));

  }
  onTorna() {
    this.router.navigate(['/dipendenti']);
  }

  onModifica() {
    this.router.navigate(['edit'], { relativeTo: this.route });
  }

}
