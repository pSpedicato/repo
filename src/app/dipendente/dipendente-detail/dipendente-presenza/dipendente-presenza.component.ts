import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatRowDef } from '@angular/material';
import { Store } from '@ngrx/store';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ExcelService } from 'src/app/presenze/presenze-list/excel.service';
import * as fromApp from '../../../store/app.reducers';
import * as fromDipendenti from '../../store/dipendente.reducers';
import * as PresenzeActions from '../../../presenze/store/presenze.actions';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Dipendente } from 'src/app/shared/dipendente.model';
import { PresenzeDataSource } from 'src/app/presenze/presenze-list/presenzeDataSource';

@Component({
  selector: 'app-dipendente-presenza',
  templateUrl: './dipendente-presenza.component.html',
  styleUrls: ['./dipendente-presenza.component.css']
})
export class DipendentePresenzaComponent implements OnInit {
  index: number;
  dipendentiState: Observable<fromDipendenti.State>;
  dataSource: PresenzeDataSource;
  dipendente: Dipendente;
  displayedColumns: string[] = ['badge', 'nome', 'cognome', 'entrata', 'uscita'];
  renderedData: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatRowDef) row: MatRowDef<any>;

  constructor(private store: Store<fromApp.AppState>, private router: Router, private xls: ExcelService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.index = +params['id'];
          this.dipendentiState = this.store.select('dipendenti');
        }
      );
    this.dipendentiState.subscribe(
      (dipendenti) => {
        this.dipendente = dipendenti.dipendenti[this.index];
      }
    );
    this.store.dispatch(new PresenzeActions.GetPresenzePersona({ id: this.dipendente.id, isOspite: false }));
    this.dataSource = new PresenzeDataSource(this.store);
    this.dataSource.loadPresenze(true);
    this.dataSource.connect().subscribe(d => this.renderedData = d);

  }


}
