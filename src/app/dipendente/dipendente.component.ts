import { Component, OnInit } from '@angular/core';
import { Dipendente } from '../shared/dipendente.model';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducers';
import * as DipendenteActions from '../dipendente/store/dipendente.actions';
import * as fromDipendenti from '../dipendente/store/dipendente.reducers';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-dipendente',
  templateUrl: './dipendente.component.html',
  styleUrls: ['./dipendente.component.css']
})

export class DipendenteComponent implements OnInit {
  dipendentiState: Observable<fromDipendenti.State>;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.store.dispatch(new DipendenteActions.GetDipendenti);
    this.dipendentiState = this.store.select('dipendenti');
  }

}
