import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Presenza } from 'src/app/shared/presenza.model';

@Component({
  selector: 'app-presenza-detail',
  templateUrl: './presenza-detail.component.html',
  styleUrls: ['./presenza-detail.component.css']
})
export class PresenzaDetailComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PresenzaDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Presenza) { }

  ngOnInit() {
  }

  onChiudi() {
    this.dialogRef.close();
  }
}
