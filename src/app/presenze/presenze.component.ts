import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';

import * as fromApp from '../store/app.reducers';
import * as DipendenteActions from '../dipendente/store/dipendente.actions';
import * as fromDipendenti from '../dipendente/store/dipendente.reducers';
import * as OspiteActions from '../ospite/store/ospite.actions';

@Component({
  selector: 'app-presenze',
  templateUrl: './presenze.component.html',
  styleUrls: ['./presenze.component.css']
})
export class PresenzeComponent implements OnInit {
  dipendentiState: Observable<fromDipendenti.State>;
  oggi: Date;
  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.oggi = new Date();
    this.store.dispatch(new OspiteActions.GetOspiti);

  }

}
