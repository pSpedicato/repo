import { Injectable } from '@angular/core';
import * as XLSX from 'xlsx';
import * as Excel from 'exceljs';
import * as fs from 'file-saver';
import * as logoFile from '../../logo64.js';
import { Dipendente } from 'src/app/shared/dipendente.model.js';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducers';
import * as DipendentiActions from '../../dipendente/store/dipendente.actions';
import { Area } from 'src/app/shared/area.model.js';

@Injectable()
export class ExcelService {
  arrayBuffer: any;
  json: any;
  constructor(private store: Store<fromApp.AppState>) {
  }

  static toExportFileName(excelFileName: string): string {
    return `${excelFileName}_export_${new Date().getTime()}.xlsx`;
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {

    // const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };

    // XLSX.writeFile(workbook, ExcelService.toExportFileName(excelFileName));
    // console.log(
    //   worksheet['A2'].s);
    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet('data report');

    const logo = workbook.addImage({
      base64: logoFile.logoBase64,
      extension: 'png',
    });

    worksheet.addImage(logo, 'A1:B3');
    const title = ['', '', 'Report presenze'];
    const titleRow = worksheet.addRow(title);

    titleRow.font = { family: 4, size: 16, color: { argb: 'A30030' }, bold: true };
    worksheet.mergeCells('C1:D2');
    const date = ['', '', 'Generato il', new Date()];
    const dateRow = worksheet.addRow(date);
    dateRow.font = { color: { argb: 'ffffff' }, italic: true };
    dateRow.alignment = { horizontal: 'left' };


    // Blank Row
    worksheet.addRow([]);


    // header
    const header = ['Entrata', 'Ora entrata', 'Uscita', 'Ora uscita',
      'Nome', 'Cognome', 'Badge', 'Area', 'Ospite', 'Nome ospite', 'Cognome ospite',
      'Badge ospite'];
    const hrow = worksheet.addRow(header);
    hrow.eachCell((cell, numbe) => {
      cell.fill = {
        type: 'pattern',
        pattern: 'solid',
        fgColor: { argb: '081c2b' },
        bgColor: { argb: 'A30030' }
      };
      cell.font = {
        color: { argb: 'FFFFFF' }
      };
      cell.border = { top: { style: 'thin' }, left: { style: 'thin' }, bottom: { style: 'thin' }, right: { style: 'thin' } };
    });
    // logo

    json.forEach(d => {
      worksheet.addRow(d);
    });
    worksheet.getColumn(1).width = 11;
    worksheet.getColumn(2).width = 11;
    worksheet.getColumn(3).width = 11;
    worksheet.getColumn(4).width = 11;
    worksheet.getColumn(5).width = 14;
    worksheet.getColumn(6).width = 14;
    worksheet.getColumn(8).width = 14;
    worksheet.getColumn(9).width = 14;
    worksheet.getColumn(10).width = 14;
    worksheet.getColumn(11).width = 14;
    worksheet.getColumn(12).width = 14;

    let i = 0;
    let j = 0;
    for (i = 1; i < 4; i++) {
      // tslint:disable-next-line:prefer-const
      let row = worksheet.getRow(i);
      for (j = 1; j < 13; j++) {
        row.getCell(j).fill = {
          type: 'pattern',
          pattern: 'solid',
          fgColor: { argb: '081c2b' },
          bgColor: { argb: 'A30030' }
        };
      }
    }

    workbook.xlsx.writeBuffer().then((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, ExcelService.toExportFileName(excelFileName));
    });
  }

  public importFromExcel(file) {
    const fileReader = new FileReader();
    fileReader.onload = () => {
      this.arrayBuffer = fileReader.result;
      const data = new Uint8Array(this.arrayBuffer);
      const arr = new Array();
      for (let i = 0; i !== data.length; ++i) {
        arr[i] = String.fromCharCode(data[i]);
      }
      const dataRead = arr.join('');
      const workbook = XLSX.read(dataRead, { type: 'binary' });
      const nomeSheet = workbook.SheetNames[0];
      const worksheet = workbook.Sheets[nomeSheet];
      const json = XLSX.utils.sheet_to_json(worksheet, { raw: true });
    };
    fileReader.readAsArrayBuffer(file);
  }
  postDip(json) {
    json.forEach(value => {
      const dip = new Dipendente(null, value['nome'], value['cognome'], value['badge'],
        value['profiloProfessionale'], value['funzAziendale'], value['sede'], value['rapporto'],
        new Area(value['areaId'], null), value['societa']);
      this.store.dispatch(new DipendentiActions.AddDipendente(dip));
    });

  }
}
