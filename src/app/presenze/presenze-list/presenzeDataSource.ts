import { DataSource } from '@angular/cdk/table';

import { BehaviorSubject, Observable, of } from 'rxjs';

import { CollectionViewer } from '@angular/cdk/collections';

import { catchError, finalize } from 'rxjs/operators';
import { Dipendente } from 'src/app/shared/dipendente.model';
import { Store } from '@ngrx/store';
import * as fromApp from '../../store/app.reducers';
import { Presenza } from 'src/app/shared/presenza.model';


export class PresenzeDataSource implements DataSource<Presenza> {

    private presenzeSubject = new BehaviorSubject<Presenza[]>([]);
    private loadingSubject = new BehaviorSubject<boolean>(false);

    public loading$ = this.loadingSubject.asObservable();

    constructor(private store: Store<fromApp.AppState>) { }

    //  connect(collectionViewer: CollectionViewer): Observable<Presenza[]> {
    connect(): Observable<Presenza[]> {
        return this.presenzeSubject.asObservable();
    }

    disconnect(collectionViewer: CollectionViewer): void {
        this.presenzeSubject.complete();
        this.loadingSubject.complete();
    }

    loadPresenze(ricerca: boolean) {
        this.loadingSubject.next(true);
        if (!ricerca) {
            this.store.select('presenze').subscribe(
                presenze => this.presenzeSubject.next(presenze.presenzeOdierne));
        } else {
            this.store.select('presenze').subscribe(
                presenze => this.presenzeSubject.next(presenze.presenzeRicerca));
        }
        this.loadingSubject.next(false);

    }

    loadPresenzeOspiti(ricerca: boolean) {
        this.loadingSubject.next(true);
        if (!ricerca) {
            this.store.select('presenze').subscribe(
                presenze => this.presenzeSubject.next(presenze.presenzeOspiti));
        } else {
            this.store.select('presenze').subscribe(
                presenze => this.presenzeSubject.next(presenze.presenzeRicerca));
        }
    }

}
