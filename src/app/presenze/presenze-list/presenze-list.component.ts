import { Component, OnInit, ViewChild, Input, OnChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { MatPaginator, MatRowDef, MatDialog, MatTableDataSource } from '@angular/material';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import * as fromApp from '../../store/app.reducers';
import * as PresenzeActions from '../store/presenze.actions';
import { PresenzeDataSource } from './presenzeDataSource';
import { ExcelService } from './excel.service';
import { map } from 'rxjs/operators';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { Presenza } from 'src/app/shared/presenza.model';
import { PresenzaDetailComponent } from '../presenza-detail/presenza-detail.component';
// import { Angular5Csv } from 'angular5-csv/Angular5-csv';
// import * as FileSaver from 'file-saver';
// import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Component({
  selector: 'app-presenze-list',
  templateUrl: './presenze-list.component.html',
  styleUrls: ['./presenze-list.component.css'],
  animations: [
    trigger('list', [
      state('in', style({
        opacity: 1,
      })),
      transition('void => *', [
        style({
          opacity: 0,
        }),
        animate(300)
      ]),
      transition('* => void', [
        animate(300, style({
          opacity: 0,
        }))
      ]),
    ]),
  ]
})
export class PresenzeListComponent implements OnInit {
  dataSource: PresenzeDataSource;
  displayedColumns: string[] = ['badge', 'nome', 'cognome', 'inizio', 'concluso', 'ospite', 'star'];
  renderedData: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatRowDef) row: MatRowDef<any>;
  @Input() ricerca: boolean;
  @Input() ospite: boolean;
  @Input() home: boolean;
  loading: boolean;
  constructor(private store: Store<fromApp.AppState>, private router: Router, private xls: ExcelService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.dataSource = new PresenzeDataSource(this.store);
    if (this.ospite) {
      this.dataSource.loadPresenzeOspiti(this.ricerca);
    } else {
      this.dataSource.loadPresenze(this.ricerca);
    }
    this.dataSource.connect().subscribe(d => this.renderedData = d);
    this.dataSource.loading$.subscribe(d => {
      this.loading = d;
    });
  }

  downloadFile(data: any) {
    const aux = this.renderedData.
      map(
        (el) => {
          const entrata = new Date(el.inizio);
          const uscita = new Date(el.fine);
          if (el.isOspite) {
            // tslint:disable-next-line:no-unused-expression
            return [
              entrata.toLocaleDateString().toString(),
              entrata.toLocaleTimeString().toString(),
              uscita.toLocaleDateString().toString(),
              uscita.toLocaleTimeString().toString(),
              el.dipendente.nome.toString(),
              el.dipendente.cognome.toString(),
              el.dipendente.badge.toString(),
              el.dipendente.area.nomeArea.toString(),
              el.isOspite.toString(),
              el.ospite.nome.toString(),
              el.ospite.cognome.toString(),
              el.ospite.societa.toString(),
            ];
          } else {
            // tslint:disable-next-line:no-unused-expression
            return [
              entrata.toLocaleDateString().toString(),
              entrata.toLocaleTimeString().toString(),
              uscita.toLocaleDateString().toString(),
              uscita.toLocaleTimeString().toString(),
              el.dipendente.nome.toString(),
              el.dipendente.cognome.toString(),
              el.dipendente.badge.toString(),
              el.dipendente.area.nomeArea.toString(),
              el.isOspite.toString(),
              ''.toString(),
              ''.toString(),
              ''
            ];
          }
        }
      );
    this.xls.exportAsExcelFile(aux, 'report-presenze');
  }
  onRowClicked(index: number) {
    this.router.navigate(['presenze', index]);
  }

  onDettagli(presenza: Presenza) {
    this.openDialog(presenza);
  }

  onElimina(index: number, presenza: Presenza) {
    this.store.dispatch(new PresenzeActions.DeletePresenza({ presenza: presenza, riga: index }));
  }
  onModifica(index: number) {
    this.store.dispatch(new PresenzeActions.StartEdit({ id: index, ospite: this.ospite }));
  }

  onUscita(presenza: Presenza) {
    presenza.fine = new Date();
    presenza.concluso = true;
    this.store.dispatch(new PresenzeActions.UpdatePresenza({ presenza: presenza }));

  }
  openDialog(presenza: Presenza): void {
    this.dialog.open(PresenzaDetailComponent, {
      width: '50%',
      data: presenza
    });
  }

}

