import { Action } from '@ngrx/store';
import { Presenza } from 'src/app/shared/presenza.model';
import { Dipendente } from 'src/app/shared/dipendente.model';
import { Ospite } from 'src/app/shared/ospite.model';
import { LineaStorico } from 'src/app/shared/presenzaOspite.model';

export const ADD_PRESENZA = 'ADD_PRESENZA';
export const GET_PRESENZE_DIPENDENTI = 'GET_PRESENZE_DIPENDENTI';
export const GET_PRESENTI = 'GET_PRESENTI';
export const GET_STORICO = 'GET_STORICO';
export const GET_PRESENZE_OSPITI = 'GET_PRESENZE_OSPITI';
export const GET_PRESENZE_PERSONA = 'GET_PRESENZE_PERSONA';
export const GET_PRESENZE_BADGE = 'GET_PRESENZE_BADGE';
export const GET_PRESENZE_CERCATE = 'GET_PRESENZE_CERCATE';
export const SET_PRESENZE_DIPENDENTI = 'SET_PRESENZE_DIPENDENTI';
export const SET_STORICO = 'SET_STORICO';
export const SET_PRESENZE_OSPITI = 'SET_PRESENZE_OSPITI';
export const SET_PRESENZE_CERCATE = 'SET_PRESENZE_CERCATE';
export const UPDATE_PRESENZA = 'UPDATE_PRESENZA';
export const DELETE_PRESENZA = 'DELETE_PRESENZA';
export const START_EDIT = 'START_EDIT';
export const STOP_EDIT = 'STOP_EDIT';

export class AddPresenza implements Action {
    readonly type = ADD_PRESENZA;
    constructor(public payload: Presenza) { }
}

export class UpdatePresenza implements Action {
    readonly type = UPDATE_PRESENZA;
    constructor(public payload: { presenza: Presenza }) { }
}

export class DeletePresenza implements Action {
    readonly type = DELETE_PRESENZA;
    constructor(public payload: { presenza: Presenza, riga: number }) { }
}

export class StartEdit implements Action {
    readonly type = START_EDIT;
    constructor(public payload: { id: number, ospite: boolean }) { }
}

export class StopEdit implements Action {
    readonly type = STOP_EDIT;
}

export class GetPresenzeDipendenti implements Action {
    readonly type = GET_PRESENZE_DIPENDENTI;
}

export class GetPresenti implements Action {
    readonly type = GET_PRESENTI;
}

export class GetStorico implements Action {
    readonly type = GET_STORICO;
}

export class GetPresenzeOspiti implements Action {
    readonly type = GET_PRESENZE_OSPITI;
}

export class GetPresenzePersona implements Action {
    readonly type = GET_PRESENZE_PERSONA;
    constructor(public payload: { id: number, isOspite: boolean }) { }
}

export class GetPresenzeBadge implements Action {
    readonly type = GET_PRESENZE_BADGE;
    constructor(public payload: { badge: number }) { }
}

export class GetPresenzeOspite implements Action {
    readonly type = GET_PRESENZE_CERCATE;
    constructor(public payload: Ospite) { }
}

export class GetPresenzeCercate implements Action {
    readonly type = GET_PRESENZE_CERCATE;
    constructor(public payload: { da: Date, a: Date }) { }
}

export class SetPresenzeDipendenti implements Action {
    readonly type = SET_PRESENZE_DIPENDENTI;
    constructor(public payload: Presenza[]) { }
}

export class SetPresenzeOspiti implements Action {
    readonly type = SET_PRESENZE_OSPITI;
    constructor(public payload: Presenza[]) { }
}

export class SetStorico implements Action {
    readonly type = SET_STORICO;
    constructor(public payload: LineaStorico[]) { }
}

export class SetPresenzeCercate implements Action {
    readonly type = SET_PRESENZE_CERCATE;
    constructor(public payload: Presenza[]) { }
}
export type PresenzeActions = AddPresenza | DeletePresenza | UpdatePresenza | StartEdit |
    StopEdit | GetPresenzeDipendenti | GetPresenti | GetPresenzeOspite | GetPresenzeBadge | GetPresenzeCercate | SetPresenzeDipendenti |
    SetPresenzeCercate | GetPresenzeOspiti | SetPresenzeOspiti | SetStorico;
