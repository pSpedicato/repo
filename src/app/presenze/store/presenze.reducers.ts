import * as PresenzeActions from './presenze.actions';
import { Presenza } from 'src/app/shared/presenza.model';
import { LineaStorico } from 'src/app/shared/presenzaOspite.model';

export interface State {
    presenzeOdierne: Presenza[];
    presenzeRicerca: Presenza[];
    presenzeOspiti: Presenza[];
    storico: LineaStorico[];
    editedPresenza: Presenza;
    editedPresenzaIndex: number;
}

const initialState: State = {
    presenzeOdierne: [],
    presenzeRicerca: [],
    presenzeOspiti: [],
    storico: [],
    editedPresenza: null,
    editedPresenzaIndex: -1,
};
export function presenzeReducer(state = initialState, action: PresenzeActions.PresenzeActions) {
    switch (action.type) {
        // case PresenzeActions.ADD_PRESENZA:
        //     let date = new Date(action.payload.inizio);
        //     if (date.setHours(0, 0, 0, 0) === (new Date()).setHours(0, 0, 0, 0)) {
        //         if (action.payload.isOspite) {
        //             return {
        //                 ...state,
        //                 presenzeOspiti: [...state.presenzeOspiti, action.payload],
        //             };
        //         } else {
        //             return {
        //                 ...state,
        //                 presenzeOdierne: [...state.presenzeOdierne, action.payload],
        //             };
        //         }
        //     } else {
        //         return {
        //             ...state
        //         };
        //     }
        case PresenzeActions.SET_PRESENZE_DIPENDENTI:
            return {
                ...state,
                presenzeOdierne: [...action.payload],
            };

        case PresenzeActions.SET_PRESENZE_OSPITI:
            return {
                ...state,
                presenzeOspiti: [...action.payload],
            };
        case PresenzeActions.SET_STORICO:
            return {
                ...state,
                storico: [...action.payload],
            };
        case PresenzeActions.SET_PRESENZE_CERCATE:
            return {
                ...state,
                presenzeRicerca: [...action.payload],
            };
        case PresenzeActions.UPDATE_PRESENZA:
            const presenza = state.presenzeOdierne[state.editedPresenzaIndex];
            const updatedPresenza = {
                ...presenza,
                ...action.payload.presenza,
            };
            const newPresenze = [...state.presenzeOdierne];
            newPresenze[state.editedPresenzaIndex] = updatedPresenza;
            return {
                ...state,
                presenzeOdierne: newPresenze,
            };
        case PresenzeActions.DELETE_PRESENZA:
            if (action.payload.presenza.isOspite) {
                const oldPresenze = [...state.presenzeOspiti];
                oldPresenze.splice(action.payload.riga, 1);
                return {
                    ...state,
                    presenzeOspiti: oldPresenze,
                    editedPresenza: null,
                    editedPresenzaIndex: -1,
                };
            } else {
                const oldPresenze = [...state.presenzeOdierne];
                oldPresenze.splice(action.payload.riga, 1);
                return {
                    ...state,
                    presenzeOdierne: oldPresenze,
                    editedPresenza: null,
                    editedPresenzaIndex: -1,
                };
            }

        case PresenzeActions.START_EDIT:
            if (!action.payload.ospite) {
                const editedPresenza = { ...state.presenzeOdierne[action.payload.id] };
                return {
                    ...state,
                    editedPresenza: editedPresenza,
                    editedPresenzaIndex: action.payload.id,
                };
            } else {
                const editedPresenza = { ...state.presenzeOspiti[action.payload.id] };
                return {
                    ...state,
                    editedPresenza: editedPresenza,
                    editedPresenzaIndex: action.payload.id,
                };
            }

        case PresenzeActions.STOP_EDIT:
            return {
                ...state,
                editedPresenza: null,
                editedPresenzaIndex: -1,
            };
        default:
            return state;
    }
}
