import { Injectable } from '@angular/core';
import { Effect, Actions } from '@ngrx/effects';
import { Router } from '@angular/router';
import * as PresenzeActions from './presenze.actions';
import * as fromPresenze from './presenze.reducers';
import { Dipendente } from '../../shared/dipendente.model';
import { switchMap, map, withLatestFrom, concat } from 'rxjs/operators';
import { from } from 'rxjs';
import { HttpClient, HttpRequest, HttpResponse, HttpParams, HttpEventType } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Presenza } from 'src/app/shared/presenza.model';
import { LineaStorico } from 'src/app/shared/presenzaOspite.model';

@Injectable()
export class PresenzeEffects {
    @Effect()
    getPresenzeAttive = this.actions$.ofType(PresenzeActions.GET_PRESENZE_DIPENDENTI)
        .pipe(
            switchMap((action: PresenzeActions.GetPresenzeDipendenti) => {
                return this.httpClient.get<Presenza[]>('https://gepre.azurewebsites.net/api/presenze/', {
                    observe: 'body',
                    params: {
                        'filter[where][and][0][inizio][gt]': new Date().setHours(0, 0, 0, 0).toString(),
                        'filter[where][and][1][inizio][lt]': new Date().setHours(24, 0, 0, 0).toString(),
                        'filter[where][and][2][concluso]': 'false',
                        'filter[where][and][3][isOspite]': 'false',
                        'filter[include][dipendente]': 'area',
                    },
                    responseType: 'json',
                });
            }), map(
                (presenze) => {
                    return {
                        type: PresenzeActions.SET_PRESENZE_DIPENDENTI,
                        payload: presenze,
                    };
                }
            )
        );
    @Effect()
    getPresenti = this.actions$.ofType(PresenzeActions.GET_PRESENTI)
        .pipe(
            switchMap((action: PresenzeActions.GetPresenti) => {
                return this.httpClient.get<Presenza[]>(
                    'https://gepre.azurewebsites.net/api/presenze?filter[include]=ospite&filter[include][dipendente]=area', {
                        observe: 'body',
                        params: {
                            'filter[where][and][0][inizio][gt]': new Date().setHours(0, 0, 0, 0).toString(),
                            'filter[where][and][1][inizio][lt]': new Date().setHours(24, 0, 0, 0).toString(),
                            'filter[where][and][2][concluso]': 'false',
                        },
                        responseType: 'json',
                    });
            }), map(
                (presenze) => {
                    return {
                        type: PresenzeActions.SET_PRESENZE_CERCATE,
                        payload: presenze,
                    };
                }
            )
        );

    @Effect()
    getPresenzeOspiti = this.actions$.ofType(PresenzeActions.GET_PRESENZE_OSPITI)
        .pipe(
            switchMap((action: PresenzeActions.GetPresenzeDipendenti) => {
                return this.httpClient.get<Presenza[]>(
                    'https://gepre.azurewebsites.net/api/presenze?filter[include]=ospite&filter[include][dipendente]=area', {
                        observe: 'body',
                        params: {
                            'filter[where][and][0][inizio][gt]': new Date().setHours(0, 0, 0, 0).toString(),
                            'filter[where][and][1][inizio][lt]': new Date().setHours(24, 0, 0, 0).toString(),
                            'filter[where][and][2][concluso]': 'false',
                            'filter[where][and][3][isOspite]': 'true',
                        },
                        responseType: 'json',
                    });
            }), map(
                (presenze) => {
                    return {
                        type: PresenzeActions.SET_PRESENZE_OSPITI,
                        payload: presenze,
                    };
                }
            )
        );

    @Effect()
    getStorico = this.actions$.ofType(PresenzeActions.GET_STORICO)
        .pipe(
            switchMap((action: PresenzeActions.GetStorico) => {
                return this.httpClient.get<LineaStorico[]>(
                    'https://gepre.azurewebsites.net/api/presenze/stats', {
                        observe: 'body',
                        params: {
                            'filter[where][inizio][gt]': new Date().setHours(0, 0, 0, 0).toString(),
                        },
                        responseType: 'json',
                    });
            }), map(
                (data) => {
                    return {
                        type: PresenzeActions.SET_STORICO,
                        payload: data,
                    };
                }
            )
        );

    @Effect()
    getPresenzePersona = this.actions$.ofType(PresenzeActions.GET_PRESENZE_PERSONA)
        .pipe(
            switchMap((action: PresenzeActions.GetPresenzePersona) => {
                if (action.payload.isOspite) {
                    return this.httpClient.get<Presenza[]>(
                        'https://gepre.azurewebsites.net/api/presenze?filter[include]=ospite&filter[include][dipendente]=area', {
                            observe: 'body',
                            params: {
                                'filter[where][ospiteId]': action.payload.id.toString(),
                                'filter[where][isOspite]': 'true',
                                'filter[order]': 'inizio DESC'
                            },
                            responseType: 'json',
                        });
                } else {
                    return this.httpClient.get<Presenza[]>(
                        'https://gepre.azurewebsites.net/api/presenze?filter[include]=ospite&filter[include][dipendente]=area', {
                            observe: 'body',
                            params: {
                                'filter[where][dipendenteId]': action.payload.id.toString(),
                                'filter[where][isOspite]': 'false',
                                'filter[order]': 'inizio DESC'
                            },
                            responseType: 'json',
                        });
                }
            }), map(
                (presenze) => {
                    return {
                        type: PresenzeActions.SET_PRESENZE_CERCATE,
                        payload: presenze,
                    };
                }
            )
        );

    @Effect()
    getPresenzeCercate = this.actions$.ofType(PresenzeActions.GET_PRESENZE_CERCATE)
        .pipe(
            switchMap((action: PresenzeActions.GetPresenzeCercate) => {
                return this.httpClient.get<Presenza[]>(
                    'https://gepre.azurewebsites.net/api/presenze?filter[include]=ospite&filter[include][dipendente]=area', {
                        observe: 'body',
                        params: {
                            'filter[where][and][0][inizio][gt]': new Date(action.payload.da).setHours(0, 0, 0, 0).toString(),
                            'filter[where][and][1][inizio][lt]': new Date(action.payload.a).setHours(24, 0, 0, 0).toString(),
                            'filter[order]': 'inizio DESC'
                        },
                        responseType: 'json',
                    });
            }), map(
                (presenze) => {
                    return {
                        type: PresenzeActions.SET_PRESENZE_CERCATE,
                        payload: presenze,
                    };
                }
            )
        );
    @Effect()
    getPresenzeBadge = this.actions$.ofType(PresenzeActions.GET_PRESENZE_BADGE)
        .pipe(
            switchMap((action: PresenzeActions.GetPresenzeBadge) => {
                return this.httpClient.get<Presenza[]>(
                    'https://gepre.azurewebsites.net/api/presenze?filter[include]=ospite&filter[include][dipendente]=area', {
                        observe: 'body',
                        params: {
                            'filter[where][or][0][badge]': action.payload.badge.toString(),
                            'filter[where][or][1][dipendente.badge]': action.payload.badge.toString(),
                            'filter[order]': 'inizio DESC'
                        },
                        responseType: 'json',
                    });
            }), map(
                (presenze) => {
                    return {
                        type: PresenzeActions.SET_PRESENZE_CERCATE,
                        payload: presenze,
                    };
                }
            )
        );
    @Effect()
    getPresenzeBadgeDipendente = this.actions$.ofType(PresenzeActions.GET_PRESENZE_BADGE)
        .pipe(
            switchMap((action: PresenzeActions.GetPresenzeBadge) => {
                return this.httpClient.get<Dipendente>(
                    'https://gepre.azurewebsites.net/api/dipendenti', {
                        observe: 'body',
                        params: {
                            'filter[where][badge]': action.payload.badge.toString(),
                        },
                        responseType: 'json',
                    });
            }), switchMap(
                (dipendente) => {
                    if (dipendente[0] != null) {
                        return this.httpClient.get<Presenza[]>(
                            'https://gepre.azurewebsites.net/api/presenze?filter[include]=ospite&filter[include][dipendente]=area', {
                                observe: 'body',
                                params: {
                                    'filter[where][dipendenteId]': dipendente[0].id.toString(),
                                    'filter[order]': 'inizio DESC'
                                },
                                responseType: 'json',
                            });
                    } else {
                        return [];
                    }

                }), map(
                    (presenze) => {
                        if (presenze !== []) {
                            return {
                                type: PresenzeActions.SET_PRESENZE_CERCATE,
                                payload: presenze,
                            };
                        }
                    }
                )
        );


    @Effect({ dispatch: false })
    addPresenze = this.actions$.ofType(PresenzeActions.ADD_PRESENZA)
        .pipe(
            switchMap((action: PresenzeActions.AddPresenza) => {
                let req;
                if (action.payload.ospite != null) {
                    req = new HttpRequest('POST', 'https://gepre.azurewebsites.net/api/presenze',
                        {
                            'inizio': action.payload.inizio,
                            'fine': action.payload.fine,
                            'concluso': action.payload.concluso,
                            'dipendenteId': action.payload.dipendente.id,
                            'ospiteId': action.payload.ospite.id,
                            'isOspite': action.payload.isOspite,
                            'badge': action.payload.badge,
                        }, { reportProgress: true });
                } else {
                    req = new HttpRequest('POST', 'https://gepre.azurewebsites.net/api/presenze',
                        {
                            'inizio': action.payload.inizio,
                            'fine': action.payload.fine,
                            'concluso': action.payload.concluso,
                            'dipendenteId': action.payload.dipendente.id,
                            'ospiteId': null,
                            'isOspite': action.payload.isOspite,
                            'badge': action.payload.badge,
                        }, { reportProgress: true });
                }
                return this.httpClient.request(req);
            }),
            map((response) => {
                if (response.type === HttpEventType.Response) {
                    if (response['status'] === 200) {
                        const body = response['body'];
                        if (!body['isOspite']) {
                            this.store.dispatch(new PresenzeActions.GetPresenzeDipendenti);
                        } else {
                            this.store.dispatch(new PresenzeActions.GetPresenzeOspiti);
                        }
                        this.store.dispatch(new PresenzeActions.GetPresenti);

                    }
                }
            })
        );

    @Effect({ dispatch: false })
    deletePresenza = this.actions$.ofType(PresenzeActions.DELETE_PRESENZA)
        .pipe(
            switchMap((action: PresenzeActions.DeletePresenza) => {
                const params = 'https://gepre.azurewebsites.net/api/presenze/' + action.payload.presenza.id.toString();
                const req = new HttpRequest('DELETE', params,
                    { reportProgress: true });
                return this.httpClient.request(req);
            }),
        );

    @Effect({ dispatch: false })
    updatePresenza = this.actions$.ofType(PresenzeActions.UPDATE_PRESENZA)
        .pipe(
            switchMap((action: PresenzeActions.UpdatePresenza) => {
                const url = 'https://gepre.azurewebsites.net/api/presenze/' + action.payload.presenza.id.toString();
                let reqPUT;
                if (action.payload.presenza.isOspite) {
                    reqPUT = new HttpRequest('PUT', url,
                        {
                            'inizio': action.payload.presenza.inizio,
                            'fine': action.payload.presenza.fine,
                            'concluso': action.payload.presenza.concluso,
                            'dipendenteId': action.payload.presenza.dipendente.id,
                            'ospiteId': action.payload.presenza.ospite.id,
                            'isOspite': action.payload.presenza.isOspite,
                            'badge': action.payload.presenza.badge,
                        }, { reportProgress: true });
                } else {
                    reqPUT = new HttpRequest('PUT', url,
                        {
                            'inizio': action.payload.presenza.inizio,
                            'fine': action.payload.presenza.fine,
                            'concluso': action.payload.presenza.concluso,
                            'dipendenteId': action.payload.presenza.dipendente.id,
                            'ospiteId': null,
                            'isOspite': action.payload.presenza.isOspite,
                            'badge': action.payload.presenza.badge,
                        }, { reportProgress: true });
                }
                return this.httpClient.request(reqPUT);
            })
        );

    constructor(private actions$: Actions, private store: Store<fromPresenze.State>, private httpClient: HttpClient) { }

}
