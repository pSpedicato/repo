import { Component, OnInit } from '@angular/core';
import * as fromApp from '../../store/app.reducers';
import * as PresenzeActions from '../store/presenze.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-presenze-home',
  templateUrl: './presenze-home.component.html',
  styleUrls: ['./presenze-home.component.css']
})
export class PresenzeHomeComponent implements OnInit {

  date = new Date();
  constructor(private store: Store<fromApp.AppState>, ) { }

  ngOnInit() {
  }
  onRicarica() {
    this.store.dispatch(new PresenzeActions.GetPresenzeDipendenti);
    this.store.dispatch(new PresenzeActions.GetPresenzeOspiti);
  }

}
