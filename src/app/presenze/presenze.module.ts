import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PresenzeComponent } from './presenze.component';
import { PresenzeEditComponent } from './presenze-edit/presenze-edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { PresenzeListComponent } from './presenze-list/presenze-list.component';
import { ExcelService } from './presenze-list/excel.service';
import { PresenzeHomeComponent } from './presenze-home/presenze-home.component';
import { Routes, RouterModule } from '@angular/router';
import { PresenzeRicercaComponent } from './presenze-ricerca/presenze-ricerca.component';
import { CercaPerDataComponent } from './presenze-ricerca/metodi-ricerca/cerca-per-data/cerca-per-data.component';
import { CercaPerDipendenteComponent } from './presenze-ricerca/metodi-ricerca/cerca-per-dipendente/cerca-per-dipendente.component';
import { PresenzaDetailComponent } from './presenza-detail/presenza-detail.component';
import { CercaPerBadgeComponent } from './presenze-ricerca/metodi-ricerca/cerca-per-badge/cerca-per-badge.component';
import { CercaPerOspiteComponent } from './presenze-ricerca/metodi-ricerca/cerca-per-ospite/cerca-per-ospite.component';

@NgModule({
  declarations: [
    PresenzeComponent,
    PresenzeEditComponent,
    PresenzeListComponent,
    PresenzaDetailComponent,
    PresenzeHomeComponent,
    PresenzeRicercaComponent,
    CercaPerDataComponent,
    CercaPerDipendenteComponent,
    PresenzaDetailComponent,
    CercaPerBadgeComponent,
    CercaPerOspiteComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
  ],
  entryComponents: [
    PresenzaDetailComponent
  ],
  exports: [
    PresenzeEditComponent,
    PresenzeListComponent
  ],
  providers: [ExcelService],
})
export class PresenzeModule { }
