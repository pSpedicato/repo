import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Dipendente } from 'src/app/shared/dipendente.model';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';
import { startWith, map } from 'rxjs/operators';

import * as fromApp from '../../../../store/app.reducers';
import * as PresenzeActions from '../../../store/presenze.actions';

@Component({
  selector: 'app-cerca-per-badge',
  templateUrl: './cerca-per-badge.component.html',
  styleUrls: ['./cerca-per-badge.component.css']
})
export class CercaPerBadgeComponent implements OnInit {

  ricercaForm: FormGroup;
  badge = new FormControl('', Validators.required);

  constructor(private store: Store<fromApp.AppState>, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.ricercaForm = new FormGroup({
      'badge': this.badge,
    });
  }

  onSubmit() {
    const value = this.ricercaForm.value;
    this.store.dispatch(new PresenzeActions.GetPresenzeBadge({ badge: value.badge }));
  }

}
