import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromApp from '../../../../store/app.reducers';
import * as PresenzeActions from '../../../store/presenze.actions';

@Component({
  selector: 'app-cerca-per-data',
  templateUrl: './cerca-per-data.component.html',
  styleUrls: ['./cerca-per-data.component.css']
})
export class CercaPerDataComponent implements OnInit {

  ricercaForm: FormGroup;
  dateDa: Date;
  dateA: Date;
  maxDate = new Date();
  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.ricercaForm = new FormGroup({
      'inizio': new FormControl(new Date, Validators.required),
      'fine': new FormControl(new Date)
    }
    );
  }

  onSubmit() {
    this.dateDa = this.ricercaForm.value.inizio;
    this.dateA = this.ricercaForm.value.fine;
    this.store.dispatch(new PresenzeActions.GetPresenzeCercate({ da: this.dateDa, a: this.dateA }));

  }

}
