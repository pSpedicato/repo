import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as fromApp from '../../../../store/app.reducers';
import * as PresenzeActions from '../../../store/presenze.actions';
import { Dipendente } from 'src/app/shared/dipendente.model';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-cerca-per-dipendente',
  templateUrl: './cerca-per-dipendente.component.html',
  styleUrls: ['./cerca-per-dipendente.component.css']
})
export class CercaPerDipendenteComponent implements OnInit {

  ricercaForm: FormGroup;
  dipendenti: Dipendente[];
  dipendente = new FormControl('', Validators.required);
  filteredOptions: Observable<Dipendente[]>;

  constructor(private store: Store<fromApp.AppState>, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.store.select('dipendenti').subscribe(
      (dipendenti) => {
        this.dipendenti = dipendenti.dipendenti;
      }
    );

    this.filteredOptions = this.dipendente.valueChanges
      .pipe(
        startWith<string | Dipendente>(''),
        map(value => typeof value === 'string' ? value : value.cognome),
        map(cognome => cognome ? this._filter(cognome) : this.dipendenti.slice())
      );

    this.ricercaForm = new FormGroup({
      'dipendente': this.dipendente,
    }
    );
  }

  // serve per disaccoppiare il value dalla stringa mostrata nella selezione del dipendente
  displayFn(dipendente?: Dipendente): string | undefined {
    return dipendente ? dipendente.nome.concat(' ', dipendente.cognome, ' - ', dipendente.badge) : undefined;
  }

  private _filter(value: string): Dipendente[] {
    const filterValue = value.toLowerCase();
    return this.dipendenti.filter(dipendente => dipendente.cognome.toLowerCase().indexOf(filterValue) === 0);
  }


  onSubmit() {
    const value = this.ricercaForm.value;
    const dip: Dipendente = value.dipendente;
    if (dip.id == null) {
      this.openSnackBar('Dipendente non valido. Clicca sul dipendente desiderato', 'ok');
    } else {
      this.store.dispatch(new PresenzeActions.GetPresenzePersona({id: dip.id, isOspite: false}));
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
