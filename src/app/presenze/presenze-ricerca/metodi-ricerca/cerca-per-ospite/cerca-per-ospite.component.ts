import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material';
import { startWith, map } from 'rxjs/operators';
import * as fromApp from '../../../../store/app.reducers';
import * as PresenzeActions from '../../../store/presenze.actions';
import { Ospite } from 'src/app/shared/ospite.model';

@Component({
  selector: 'app-cerca-per-ospite',
  templateUrl: './cerca-per-ospite.component.html',
  styleUrls: ['./cerca-per-ospite.component.css']
})
export class CercaPerOspiteComponent implements OnInit {


  ricercaForm: FormGroup;
  ospiti: Ospite[];
  ospite = new FormControl('', Validators.required);
  filteredOptions: Observable<Ospite[]>;

  constructor(private store: Store<fromApp.AppState>, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.store.select('ospiti').subscribe(
      (ospiti) => {
        this.ospiti = ospiti.ospiti;
      }
    );

    this.filteredOptions = this.ospite.valueChanges
      .pipe(
        startWith<string | Ospite>(''),
        map(value => typeof value === 'string' ? value : value.cognome),
        map(cognome => cognome ? this._filter(cognome) : this.ospiti.slice())
      );

    this.ricercaForm = new FormGroup({
      'ospite': this.ospite,
    }
    );
  }

  // serve per disaccoppiare il value dalla stringa mostrata nella selezione del dipendente
  displayFn(ospite?: Ospite): string | undefined {
    return ospite ? ospite.nome.concat(' ', ospite.cognome, ' - ', ospite.societa) : undefined;
  }

  private _filter(value: string): Ospite[] {
    const filterValue = value.toLowerCase();
    return this.ospiti.filter(ospite => ospite.cognome.toLowerCase().indexOf(filterValue) === 0);
  }


  onSubmit() {
    const value = this.ricercaForm.value;
    const osp: Ospite = value.ospite;
    if (osp.id == null) {
      this.openSnackBar("Ospite non valido. Clicca sull'ospite desiderato", 'ok');
    } else {
      this.store.dispatch(new PresenzeActions.GetPresenzePersona({ id: osp.id, isOspite: true }));
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }
}
