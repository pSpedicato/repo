import { Component, OnInit } from '@angular/core';
import * as fromApp from '../../store/app.reducers';
import * as PresenzeActions from '../store/presenze.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-presenze-ricerca',
  templateUrl: './presenze-ricerca.component.html',
  styleUrls: ['./presenze-ricerca.component.css']
})
export class PresenzeRicercaComponent implements OnInit {

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {

  }

  onVisualizzaTutti() {
    this.store.dispatch(new PresenzeActions.GetPresenti);
  }
}
