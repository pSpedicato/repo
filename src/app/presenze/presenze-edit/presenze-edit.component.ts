import { Component, OnInit, OnDestroy, Input, OnChanges } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { take, startWith, map, tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Dipendente } from 'src/app/shared/dipendente.model';
import * as fromApp from '../../store/app.reducers';
import { Observable, Subscription } from 'rxjs';
import * as PresenzeActions from '../store/presenze.actions';
import { Presenza } from 'src/app/shared/presenza.model';
import { MatSnackBar } from '@angular/material';
import { Ospite } from 'src/app/shared/ospite.model';

@Component({
  selector: 'app-presenze-edit',
  templateUrl: './presenze-edit.component.html',
  styleUrls: ['./presenze-edit.component.css'],
})

export class PresenzeEditComponent implements OnInit, OnDestroy, OnChanges {
  presenzeForm: FormGroup;
  editMode = false;
  editPresenza: Presenza;
  minDate = new Date();
  idOnDb: number;
  dipendenti: Dipendente[];
  ospiti: Ospite[];
  hours = new Array(24);
  minutes = new Array(12);
  conclusa = false;
  isOspite = false;
  subscription: Subscription;
  dipendenteControl = new FormControl('', Validators.required);
  ospiteControl = new FormControl('');
  badgeControl = new FormControl('');
  filteredDipendenti: Observable<Dipendente[]>;
  filteredOspiti: Observable<Ospite[]>;

  constructor(private router: Router, private route: ActivatedRoute, private store: Store<fromApp.AppState>,
    public snackBar: MatSnackBar) { }
  ngOnChanges() {
  }
  ngOnInit() {
    this.subscription = this.store.select('presenze')
      .subscribe(
        (data) => {
          if (data.editedPresenzaIndex > -1) {
            this.editPresenza = data.editedPresenza;
            this.editMode = true;
          } else {
            this.editMode = false;
          }
          this.ospiteControl.setValidators(null);
          this.badgeControl.setValidators(null);
          this.init();
        }
      );
    // popolo ore e minuti
    let i;
    for (i = 0; i < 24; i++) {
      this.hours[i] = i;
    }
    let j = 0;
    for (i = 0; i < 12; i++) {
      this.minutes[i] = j;
      j = j + 5;
    }
    this.store.select('dipendenti').subscribe(
      (dipendenti) => {
        this.dipendenti = dipendenti.dipendenti;
      }
    );
    this.store.select('ospiti').subscribe(
      (ospiti) => {
        this.ospiti = ospiti.ospiti;
      }
    );

    this.filteredDipendenti = this.dipendenteControl.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        startWith<string | Dipendente>(''),
        map(value => typeof value === 'string' ? value : value.badge),
        map(badge => badge ? this._filterDipendenti(badge) : this.dipendenti.slice())
      );

    this.filteredOspiti = this.ospiteControl.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        startWith<string | Ospite>(''),
        map(value => typeof value === 'string' ? value : value.cognome),
        map(cognome => cognome ? this._filterOspiti(cognome) : this.ospiti.slice())
      );
  }


  ngOnDestroy() {
    this.store.dispatch(new PresenzeActions.StopEdit());
    this.subscription.unsubscribe();
  }


  // serve per disaccoppiare il value dalla stringa mostrata nella selezione del dipendente
  displayFn(dipendente?: Dipendente): string | undefined {
    return dipendente ? dipendente.nome.concat(' ', dipendente.cognome, ' - ', dipendente.badge) : undefined;
  }
  displayFnOspite(ospite?: Ospite): string | undefined {
    return ospite ? ospite.nome.concat(' ', ospite.cognome, ' - ', ospite.societa) : undefined;
  }


  init() {
    let inizio = new Date();
    let fine = null;
    let oraInizio;
    let oraFine;
    let minutoInizio;
    let minutoFine;
    let conclusa = false;

    oraInizio = inizio.getHours();
    minutoInizio = Math.round(inizio.getMinutes() / 5) * 5;


    if (this.editMode) {
      this.dipendenteControl.setValue(this.editPresenza.dipendente);
      inizio = new Date(this.editPresenza.inizio);
      if (this.editPresenza.fine != null) {
        fine = new Date(this.editPresenza.fine);
        oraFine = fine.getHours();
        minutoFine = fine.getMinutes();
      } else {
        fine = new Date();
        oraFine = fine.getHours();
        minutoFine = Math.round(fine.getMinutes() / 5) * 5;
      }
      oraInizio = inizio.getHours();
      minutoInizio = inizio.getMinutes();
      conclusa = this.editPresenza.concluso;
      this.isOspite = this.editPresenza.isOspite;
      this.ospiteControl.setValue(this.editPresenza.ospite);
      this.badgeControl.setValue(this.editPresenza.badge);
    }
    this.presenzeForm = new FormGroup({
      'dipendente': this.dipendenteControl,
      'inizio': new FormControl(inizio, Validators.required),
      'oraInizio': new FormControl(oraInizio, Validators.required),
      'minutoInizio': new FormControl(minutoInizio, Validators.required),
      'fine': new FormControl(fine),
      'oraFine': new FormControl(oraFine),
      'minutoFine': new FormControl(minutoFine),
      'conclusa': new FormControl(conclusa),
      'isOspite': new FormControl(this.isOspite),
      'ospite': this.ospiteControl,
      'badge': this.badgeControl,
    });
    if (this.isOspite) {
      this.ospiteControl.setValidators(Validators.required);
      this.badgeControl.setValidators(Validators.required);
    }
  }

  private _filterDipendenti(value: string): Dipendente[] {
    const filterValue = value.toLowerCase();
    return this.dipendenti.filter(dipendente => dipendente.badge.toLowerCase().indexOf(filterValue) === 0);
  }
  private _filterOspiti(value: string): Ospite[] {
    const filterValue = value.toLowerCase();
    return this.ospiti.filter(ospite => ospite.cognome.toLowerCase().indexOf(filterValue) === 0);
  }
  onSubmit() {
    const value = this.presenzeForm.value;
    value.inizio.setHours(value.oraInizio, value.minutoInizio, 0, 0);
    if (value.fine != null) {
      value.fine.setHours(value.oraFine, value.minutoFine, 0, 0);
    }
    const dip: Dipendente = value.dipendente;
    if (dip.nome == null) {
      this.openSnackBar('Dipendente non valido. Clicca sul dipendente desiderato', 'ok');
    } else {
      const presenza = new Presenza(null, value.inizio, dip, value.fine, false, null, null);
      presenza.concluso = this.conclusa;
      if (this.isOspite) {
        const osp: Ospite = value.ospite;
        if (osp.id == null) {
          // tslint:disable-next-line:quotemark
          this.openSnackBar("Ospite non valido. Clicca sull'ospite desiderato", 'ok');
        } else {
          presenza.badge = value.badge;
          presenza.ospite = osp;
          presenza.isOspite = true;
          this.onSubmitPresenza(presenza);

        }
      } else {
        this.onSubmitPresenza(presenza);

      }
    }
  }

  onSubmitPresenza(presenza) {

    if (this.editMode) {
      presenza.id = this.editPresenza.id;
      this.store.dispatch(new PresenzeActions.UpdatePresenza({ presenza: presenza }));
    } else {
      this.store.dispatch(new PresenzeActions.AddPresenza(presenza));
    }
    this.store.dispatch(new PresenzeActions.StopEdit);
    this.editMode = false;
    this.presenzeForm.reset();

    this.ospiteControl.setValue('');
    this.dipendenteControl.setValue('');
  }

  onClear() {
    this.presenzeForm.reset();
    this.ospiteControl.setValue('');
    this.dipendenteControl.setValue('');
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3000,
    });
  }

}
