export class Area {
    public id: number;
    public nomeArea: string;
    constructor(id: number, nomeArea: string) {
        this.id = id;
        this.nomeArea = nomeArea;
    }
}
