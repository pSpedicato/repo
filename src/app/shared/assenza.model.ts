import { Dipendente } from './dipendente.model';

export class Assenza {
    public id: number;
    public inizio: Date;
    public fine: Date;
    public dipendente: Dipendente;
    public tipo: string;
    public motivo: string;
    public luogo: string;

    constructor(id: number, inizio: Date, dipendente: Dipendente, fine: Date, tipo: string, motivo: string, luogo: string) {
        this.id = id;
        this.inizio = inizio;
        this.fine = fine;
        this.dipendente = dipendente;
        this.tipo = tipo;
        this.motivo = motivo;
        this.luogo = luogo;
    }
}
