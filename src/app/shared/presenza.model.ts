import { Dipendente } from './dipendente.model';
import { Ospite } from './ospite.model';

export class Presenza {
    public id: number;
    public inizio: Date;
    public fine: Date;
    public concluso: boolean;
    public dipendente: Dipendente;
    public ospite: Ospite;
    public isOspite: boolean;
    public badge: string;
    constructor(id: number, inizio: Date, dipendente: Dipendente, fine: Date, isOspite: boolean, ospite: Ospite, badge: string) {
        this.id = id;
        this.inizio = inizio;
        this.fine = fine;
        this.dipendente = dipendente;
        this.concluso = false;
        this.isOspite = isOspite;
        this.ospite = ospite;
        this.badge = badge;
    }
}
