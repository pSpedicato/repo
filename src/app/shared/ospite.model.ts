export class Ospite {
    public id: number;
    public nome: string;
    public cognome: string;
    public documentoID: string;
    public societa: string;

    constructor(id: number, nome: string, cognome: string, documentoId: string, societa: string) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.documentoID = documentoId;
        this.societa = societa;
    }
}
