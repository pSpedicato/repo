export class User {
    public id: number;
    public nome: string;
    public cognome: string;
    public email: string;
    public password: string;
    public firstAccess: boolean;
    public amministratore: boolean;

    constructor(id: number, nome: string, cognome: string, email: string, password: string, firstAccess: boolean, amministratore: boolean) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.password = password;
        this.firstAccess = firstAccess;
        this.amministratore = amministratore;
    }
}
