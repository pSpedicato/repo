import { Dipendente } from './dipendente.model';
import { Ospite } from './ospite.model';

export class LineaStorico {
    public count: number;
    public date: Date;
    constructor(count: number, date: Date) {
        this.count = count;
        this.date = date;
    }
}
