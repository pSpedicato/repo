import { Presenza } from './presenza.model';
import { Area } from './area.model';

export class Dipendente {
    public id: number;
    public nome: string;
    public cognome: string;
    public badge: string;
    public profiloProfessionale: string;
    public funzAziendale: string;
    public sede: string;
    public rapporto: string;
    public area: Area;
    public societa: string;
    public presenza: Presenza[];
    constructor(id: number, nome: string, cognome: string, badge: string,
        profiloProfessionale: string, funzioneAziendale: string, sede: string,
        rapportoLavorativo: string, area: Area, societa: string) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.badge = badge;
        this.profiloProfessionale = profiloProfessionale;
        this.funzAziendale = funzioneAziendale;
        this.sede = sede;
        this.rapporto = rapportoLavorativo;
        this.area = area;
        this.societa = societa;
    }
}
