import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import * as PresenzeActions from '../presenze/store/presenze.actions';
import * as fromApp from '../store/app.reducers';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  /** Based on the screen size, switch from standard to one column per row */
  date = new Date();
  cards = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map(({ matches }) => {
      if (matches) {
        return [
          { title: '', cols: 1, rows: 1, content: 2 },
          { title: '', cols: 1, rows: 1, content: 3 },

        ];
      }

      return [
        { title: '', cols: 1, rows: 1, content: 2 },
        { title: '', cols: 1, rows: 1, content: 3 },

      ];
    })
  );
  ngOnInit() {
    this.store.dispatch(new PresenzeActions.GetPresenti);
    // let date = new Date();
    // console.log(date);
    // date.setSeconds(1209600, 0);
    // console.log(date);
  }

  constructor(private breakpointObserver: BreakpointObserver, private store: Store<fromApp.AppState>) { }
}
