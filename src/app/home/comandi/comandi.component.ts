import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comandi',
  templateUrl: './comandi.component.html',
  styleUrls: ['./comandi.component.css']
})
export class ComandiComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  onDipendente() {
    this.router.navigate(['dipendenti', 'new']);
  }
  onPresenza() {
    this.router.navigate(['presenze']);
  }
  onOspite() {
    this.router.navigate(['ospiti', 'new']);
  }
  onReport() {
    this.router.navigate(['presenze', 'report']);
  }
}
