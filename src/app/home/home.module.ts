import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material.module';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { PresenzeModule } from '../presenze/presenze.module';
import { ContatoreComponent } from './contatore/contatore.component';
import { ComandiComponent } from './comandi/comandi.component';
import { GraficoComponent } from './grafico/grafico.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    HomeComponent,
    ContatoreComponent,
    ComandiComponent,
    GraficoComponent
  ],
  imports: [
    RouterModule,
    CommonModule,
    MaterialModule,
    ChartsModule,
    PresenzeModule,

  ],
  providers: [],
})
export class HomeModule { }
