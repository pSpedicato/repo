import { Component, OnInit } from '@angular/core';
import * as fromApp from '../../store/app.reducers';
import * as PresenzeActions from '../../presenze/store/presenze.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-contatore',
  templateUrl: './contatore.component.html',
  styleUrls: ['./contatore.component.css']
})
export class ContatoreComponent implements OnInit {
  numeroOspiti: number;
  numeroDipendenti: number;

  constructor(private store: Store<fromApp.AppState>) { }

  ngOnInit() {
    this.store.select('presenze').subscribe(
      (presenze) => {
        this.numeroOspiti = presenze.presenzeOspiti.length;
        this.numeroDipendenti = presenze.presenzeOdierne.length;
      }
    );
  }

}
