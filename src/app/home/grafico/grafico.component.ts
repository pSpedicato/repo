import { Component, OnInit } from '@angular/core';
import * as fromApp from '../../store/app.reducers';
import * as PresenzeActions from '../../presenze/store/presenze.actions';
import { Store } from '@ngrx/store';
import { LineaStorico } from 'src/app/shared/presenzaOspite.model';

@Component({
  selector: 'app-grafico',
  templateUrl: './grafico.component.html',
  styleUrls: ['./grafico.component.css']
})
export class GraficoComponent implements OnInit {
  public chartType = 'bar';

  public data: LineaStorico[];

  public chartDatasets: Array<any> = [
    { data: [], label: 'presenze' }
  ];

  public chartLabels: Array<any> = [];

  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(163,0,48,0.2)',
      borderColor: 'rgba(163,0,48,1)',
      pointBackgroundColor: 'rgba(163,0,48,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(163,0,48,0.8)'
    },
  ];

  public chartOptions: any = {
    responsive: true,
  };
  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }
  constructor(private store: Store<fromApp.AppState>) { }
  ngOnInit(): void {
    this.store.dispatch(new PresenzeActions.GetStorico);
    this.store.select('presenze').subscribe(
      (presenze) => {
        this.data = presenze.storico;
        this.init();
      }
    );
  }
  init() {
    let i = 0;
    for (i = 0; i < this.data.length; i++) {
      this.chartDatasets[0].data[i] = this.data[i].count;
      this.chartLabels[i] = this.data[i].date;
    }
  }
}
