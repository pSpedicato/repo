import { ActionReducerMap } from '@ngrx/store';
import * as fromDipendenti from '../dipendente/store/dipendente.reducers';
import * as fromOspiti from '../ospite/store/ospite.reducers';
import * as fromPresenze from '../presenze/store/presenze.reducers';
import * as fromArea from '../area/store/area.reducers';
import * as fromAssenze from '../assenza/store/assenze.reducers';

export interface AppState {
    dipendenti: fromDipendenti.State;
    ospiti: fromOspiti.State;
    presenze: fromPresenze.State;
    aree: fromArea.State;
    assenze: fromAssenze.State;

}
export const reducers: ActionReducerMap<AppState> = {
    dipendenti: fromDipendenti.dipendenteReducer,
    ospiti: fromOspiti.opsiteReducer,
    presenze: fromPresenze.presenzeReducer,
    aree: fromArea.areaReducer,
    assenze: fromAssenze.assenzeReducer
};
