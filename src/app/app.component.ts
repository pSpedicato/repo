import { Component, OnInit } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { Store } from '@ngrx/store';

import * as fromApp from './store/app.reducers';
import * as DipendenteActions from './dipendente/store/dipendente.actions';
import * as PresenzeActions from './presenze/store/presenze.actions';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from './auth/service/auth.service';
import { User } from './shared/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  user: User;
  nome: string;
  title = 'g-presenze-mat';
  route: string;
  authenticated = false;
  constructor(private store: Store<fromApp.AppState>, private routes: ActivatedRoute, private authService: AuthService) { }

  ngOnInit() {
    this.store.dispatch(new DipendenteActions.GetDipendenti);
    this.store.dispatch(new PresenzeActions.GetPresenzeDipendenti);
    this.store.dispatch(new PresenzeActions.GetPresenzeOspiti);
    this.route = this.routes.snapshot.toString();

    if (sessionStorage.getItem('tk')) {
      this.authenticated = true;
      this.nome = sessionStorage.getItem('username');
    }
    this.authService.event.subscribe(
      (user) => {
        if (user.user != null) {
          this.user = user.user;
          this.authenticated = true;
          this.nome = user.user.nome;
        } else {
          this.user = null;
          this.authenticated = false;
        }
      }
    );
  }

  isAuthenticated() {

  }
}
